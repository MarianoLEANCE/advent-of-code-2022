package com.adventofcode.common;

import io.vavr.collection.Array;
import io.vavr.collection.Map;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CombinationUtilsTest {

    @Test
    void combinations_2d6() {
        Array<Integer> d6 = Array.rangeClosed(1, 6);

        Array<Array<Integer>> combinations = CombinationUtils.combinations(d6, 2);

        assertEquals(Math.pow(d6.size(), 2), combinations.size());
        d6.forEach(a -> d6.forEach(b -> assertTrue(combinations.contains(Array.of(a, b)))));
    }

    @Test
    void combinations_3ab() {
        Array<String> ab = Array.of("a", "b");

        Array<Array<String>> combinations = CombinationUtils.combinations(ab, 3);

        assertEquals(Math.pow(ab.size(), 3), combinations.size());
        ab.forEach(a -> ab.forEach(b -> ab.forEach(c ->
                assertTrue(combinations.contains(Array.of(a, b, c))))));
    }

    @Test
    void combinationsCount_2of2() {
        Array<String> colors = Array.of("red", "blue");

        Array<Map<String, Integer>> combinations = CombinationUtils.combinationsCount(colors, 2);

        assertEquals(3, combinations.size());
    }

    @Test
    void combinationsCount_3of3() {
        Array<String> colors = Array.of("red", "blue", "green");

        Array<Map<String, Integer>> combinations = CombinationUtils.combinationsCount(colors, 3);

        assertEquals(10, combinations.size());
    }

}
