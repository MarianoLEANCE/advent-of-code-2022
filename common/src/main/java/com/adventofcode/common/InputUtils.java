package com.adventofcode.common;

import io.vavr.control.Try;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class InputUtils {

    public static Try<String> readFromClassPath(String file) {
        return Try.of(() -> {
            Class<InputUtils> clazz = InputUtils.class;
            InputStream inputStream = clazz.getClassLoader().getResourceAsStream(file);
            return readFromInputStream(inputStream);
        });
    }

    private static String readFromInputStream(InputStream inputStream) throws IOException {
        StringBuilder resultStringBuilder = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = br.readLine()) != null) {
                resultStringBuilder.append(line).append("\n");
            }
        }
        return resultStringBuilder.toString();
    }

}
