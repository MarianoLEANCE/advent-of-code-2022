package com.adventofcode.common;

import io.vavr.Function1;
import io.vavr.collection.Array;

public class StringUtils {

    public static Function1<String, Array<String>> split(String pattern) {
        return s -> Array.of(s.split(pattern));
    }

    public static Function1<String, String> replace(CharSequence target, CharSequence replacement) {
        return s -> s.replace(target, replacement);
    }

}
