package com.adventofcode.common;

import io.vavr.collection.Array;
import io.vavr.collection.HashMap;
import io.vavr.collection.Map;

public class CombinationUtils {

    public static <T> Array<Array<T>> combinations(Array<T> dictionary, int size) {
        if (size == 1) {
            return dictionary.map(Array::of);
        }
        Array<Array<T>> combinations = combinations(dictionary, size - 1);
        return dictionary.flatMap(a -> combinations.map(b -> b.prepend(a)));
    }

    public static <T> Array<Map<T, Integer>> combinationsCount(Array<T> dictionary, int size) {
        if (dictionary.size() == 1) {
            return Array.of(HashMap.of(dictionary.head(), size));
        }
        T head = dictionary.head();
        return Array.rangeClosed(0, size)
                .flatMap(i ->
                        combinationsCount(dictionary.subSequence(1), size - i)
                                .map(sub -> sub.put(head, i)));
    }

}
