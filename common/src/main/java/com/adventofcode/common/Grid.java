package com.adventofcode.common;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.*;
import java.util.function.Predicate;

public class Grid<T> {

    private final List<List<Cell<T>>> items;

    public Grid(int rows, int columns) {
        items = new ArrayList<>();
        for (int row = 0; row < rows; row++) {
            items.add(new ArrayList<>());
            for (int col = 0; col < columns; col++) {
                items.get(row).add(new Cell<>());
                if (col > 0) {
                    items.get(row).get(col).setLeft(items.get(row).get(col - 1));
                    items.get(row).get(col - 1).setRight(items.get(row).get(col));
                }
                if (row > 0) {
                    items.get(row).get(col).setUp(items.get(row - 1).get(col));
                    items.get(row - 1).get(col).setDown(items.get(row).get(col));
                }
            }
        }
    }

    public void set(int row, int col, T value) {
        items.get(row).get(col).setValue(value);
    }

    public T get(int row, int col) {
        return items.get(row).get(col).getValue();
    }

    public boolean contains(T value) {
        return items.stream()
                .flatMap(Collection::stream)
                .map(Cell::getValue)
                .anyMatch(value::equals);
    }

    public Optional<Cell<T>> find(T value) {
        return items.stream()
                .flatMap(Collection::stream)
                .filter(cell -> cell.getValue().equals(value))
                .findAny();
    }

    public List<Cell<T>> find(Predicate<Cell<T>> predicate) {
        return items.stream().flatMap(List::stream).filter(predicate).toList();
    }

    public int count(T value) {
        return (int) items.stream()
                .flatMap(Collection::stream)
                .map(Cell::getValue)
                .filter(value::equals)
                .count();
    }

    @Getter
    @Setter
    @EqualsAndHashCode(of = {"uuid"})
    public static class Cell<T> {

        private final UUID uuid = UUID.randomUUID();

        private T value;
        private Cell<T> up;
        private Cell<T> right;
        private Cell<T> down;
        private Cell<T> left;

    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (List<Cell<T>> item : items) {
            for (Cell<T> tCell : item) {
                builder.append(tCell.getValue());
            }
            builder.append("\n");
        }
        return builder.toString();
    }

}
