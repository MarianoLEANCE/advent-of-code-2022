package com.adventofcode.common;

import io.vavr.Function1;
import io.vavr.collection.Array;

import java.util.function.Predicate;

public class ArrayUtils {

    public static <A, B> Function1<Array<A>, Array<B>> map(Function1<A, B> f) {
        return array -> array.map(f);
    }

    public static <A> Function1<Array<A>, Array<A>> filter(Predicate<A> f) {
        return array -> array.filter(f);
    }

}
