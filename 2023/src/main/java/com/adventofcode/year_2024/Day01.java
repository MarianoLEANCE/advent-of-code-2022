package com.adventofcode.year_2024;

import io.vavr.Function1;
import io.vavr.Tuple2;
import io.vavr.collection.Array;
import io.vavr.collection.Set;
import io.vavr.collection.TreeSet;

public class Day01 {

    public int part1(String input) {
        return Function1.<String, String[]>of(x -> x.split("\n"))
                .andThen(Array::of)
                .andThen(array -> array.map(this::calibrationValue).sum())
                .andThen(Number::intValue)
                .apply(input);
    }

    public int part2(String input) {
        return Function1.<String, String[]>of(x -> x.split("\n"))
                .andThen(Array::of)
                .andThen(array -> array.map(this::completeCalibrationValue).sum())
                .andThen(Number::intValue)
                .apply(input);
    }

    private int calibrationValue(String s) {
        Array<Character> digits = Array.ofAll(s.toCharArray()).filter(Character::isDigit);
        String first = digits.head().toString();
        String last = digits.last().toString();
        return Integer.parseInt(first + last);
    }

    private int completeCalibrationValue(String s) {
        Set<String> numbers = TreeSet.of("1", "2", "3", "4", "5", "6", "7", "8", "9",
                "one", "two", "three", "four", "five", "six", "seven", "eight", "nine");

        String first = numbers.filter(s::contains)
                .toSortedMap(number -> new Tuple2<>(s.indexOf(number), number))
                .head()
                .map(x -> x, this::replace)
                ._2();
        String last = numbers.filter(s::contains)
                .toSortedMap(number -> new Tuple2<>(s.lastIndexOf(number), number))
                .last()
                .map(x -> x, this::replace)
                ._2();
        return Integer.parseInt(first + last);
    }

    private String replace(String s) {
        return s.replace("one", "1")
                .replace("two", "2")
                .replace("three", "3")
                .replace("four", "4")
                .replace("five", "5")
                .replace("six", "6")
                .replace("seven", "7")
                .replace("eight", "8")
                .replace("nine", "9");
    }

}
