package com.adventofcode.year_2024;

import io.vavr.Function1;
import io.vavr.collection.Array;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day02 {

    public int part1(String input) {
        return Function1.<String, String[]>of(x -> x.split("\n"))
                .andThen(Array::of)
                .andThen(array -> array.map(this::parse)
                        .filter(this::filter)
                        .map(CubeGame::id)
                        .sum()
                        .intValue())
                .apply(input);
    }

    public int part2(String input) {
        return Function1.<String, String[]>of(x -> x.split("\n"))
                .andThen(Array::of)
                .andThen(array -> array.map(this::parse)
                        .map(CubeGame::power)
                        .sum()
                        .intValue())
                .apply(input);
    }

    private CubeGame parse(String s) {
        int id = parseId(s);
        int maxRedColorCubeCount = parseCubeColorCount("red", s);
        int maxGreenColorCubeCount = parseCubeColorCount("green", s);
        int maxBlueCubeCount = parseCubeColorCount("blue", s);
        return new CubeGame(id, maxRedColorCubeCount, maxGreenColorCubeCount, maxBlueCubeCount);
    }

    private boolean filter(CubeGame game) {
        return game.redCubeCount <= 12
               && game.greenCubeCount <= 13
               && game.blueCubeCount <= 14;
    }

    private int parseId(String s) {
        Pattern p = Pattern.compile("Game (\\d+)");
        Matcher m = p.matcher(s);
        if (m.find()) {
            String id = m.group(1);
            return Integer.parseInt(id);
        } else {
            return 0;
        }
    }

    private int parseCubeColorCount(String color, String s) {
        Pattern p = Pattern.compile("(\\d+) " + color);
        Matcher m = p.matcher(s);
        List<Integer> cubeColorCount = new ArrayList<>();
        while (m.find()) {
            cubeColorCount.add(Integer.parseInt(m.group(1)));
        }
        return cubeColorCount.stream().mapToInt(x -> x).max().orElse(0);
    }

    record CubeGame(int id, int redCubeCount, int greenCubeCount, int blueCubeCount) {

        public int power() {
            return redCubeCount * greenCubeCount * blueCubeCount;
        }

    }

}
