package com.adventofcode.year_2024;

import com.adventofcode.common.InputUtils;
import io.vavr.control.Try;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day02Test {

    Day02 day = new Day02();

    @Test
    void part1() {
        int expected = input().map(day::part1).get();
        assertEquals(3059, expected);
    }

    @Test
    void part2() {
        int expected = input().map(day::part2).get();
        assertEquals(65371, expected);
    }

    private Try<String> input() {
        return InputUtils.readFromClassPath("day02.txt");
    }

}
