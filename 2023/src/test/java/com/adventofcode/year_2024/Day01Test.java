package com.adventofcode.year_2024;

import com.adventofcode.common.InputUtils;
import io.vavr.control.Try;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day01Test {

    Day01 day01 = new Day01();

    @Test
    void part1() {
        int expected = input().map(day01::part1).get();
        assertEquals(56042, expected);
    }

    @Test
    void part2() {
        int expected = input().map(day01::part2).get();
        assertEquals(55358, expected);
    }

    private Try<String> input() {
        return InputUtils.readFromClassPath("day01.txt");
    }

}
