package com.adventofcode.year_2024;

import io.vavr.Function1;
import io.vavr.Tuple2;
import io.vavr.collection.Array;
import io.vavr.collection.Map;

public class Day09 {

    public long part1(String input) {
        return Function1.of(this::decompress)
                .andThen(this::fragment)
                .andThen(this::checksum)
                .apply(input);
    }

    private Array<Integer> decompress(String input) {
        if (input.contains("\n")) {
            return decompress(input.replace("\n", ""));
        }
        return Array.range(0, input.length())
                .map(index -> index % 2 == 0
                        ? Array.range(0, parseIntAt(input, index)).map(x -> index / 2)
                        : Array.range(0, parseIntAt(input, index)).<Integer>map(x -> null))
                .reduce(Array::appendAll);
    }

    private int parseIntAt(String input, int index) {
        return Integer.parseInt(String.valueOf(input.charAt(index)));
    }

    private Array<Integer> fragment(Array<Integer> disk) {
        Array<Tuple2<Integer, Integer>> zipped = disk.zipWithIndex();
        Array<Integer> fileIndexes = zipped.filter(x -> x._1() != null).map(Tuple2::_2);
        Array<Integer> freeSpaceIndexes = zipped.filter(x -> x._1() == null).map(Tuple2::_2);

        Map<Integer, Integer> swaps = fileIndexes.reverse().zip(freeSpaceIndexes)
                .filter(indexes -> indexes._1() > indexes._2())
                .flatMap(swap -> Array.of(swap, swap.swap()))
                .toMap(Tuple2::_1, Tuple2::_2);

        return Array.range(0, disk.size()).map(x -> disk.get(swaps.get(x).getOrElse(x)));
    }

    private String print(Array<Integer> disk) {
        StringBuilder sb = new StringBuilder();
        disk.map(x -> x == null ? "." : "[" + x + "]").forEach(sb::append);
        return sb.toString();
    }

    private Long checksum(Array<Integer> disk) {
        return Array.range(0, disk.size())
                .map(i -> disk.get(i) == null ? 0 : i * disk.get(i))
                .sum()
                .longValue();
    }

}
