package com.adventofcode.year_2024;

import io.vavr.Function1;
import io.vavr.Function3;
import io.vavr.Tuple2;
import io.vavr.collection.Array;

import java.util.HashMap;
import java.util.Map;

public class Day11 {

    public long part1(String input) {
        return Function1.of(this::parse)
                .andThen(Function3.of(this::blink).memoized().curried().apply(new HashMap<>()).apply(25))
                .apply(input);
    }

    private Array<Long> parse(String input) {
        return Array.of(input.replace("\n", "").split(" ")).map(Long::parseLong);
    }

    private long blink(Map<Tuple2<Integer, Array<Long>>, Long> cache, Integer remaining, Array<Long> stones) {
        if (cache.containsKey(new Tuple2<>(remaining, stones))) {
            return cache.get(new Tuple2<>(remaining, stones));
        }

        if (remaining == 0) {
            return stones.size();
        }

        if (stones.size() == 1) {
            Long stone = stones.head();
            if (stone == 0) {
                Array<Long> next = Array.of(1L);
                long res = blink(cache, remaining-1, next);
                cache.put(new Tuple2<>(remaining - 1, next), res);
                return res;
            }
            if (stone.toString().length() % 2 == 0) {
                Array<Long> next = cut(stone);
                long res = blink(cache, remaining-1, next);
                cache.put(new Tuple2<>(remaining - 1, next), res);
                return res;
            }
            Array<Long> next = Array.of(2024 * stone);
            long res = blink(cache, remaining-1, next);
            cache.put(new Tuple2<>(remaining - 1, next), res);
            return res;
        }

        return stones.map(stone -> blink(cache, remaining, Array.of(stone))).sum().longValue();
    }

    private Array<Long> cut(Long x) {
        int half = x.toString().length() / 2;
        long position = (long) Math.pow(10, half);
        return Array.of((x / position), (x % position));
    }

    public long part2(String input) {
        return Function1.of(this::parse)
                .andThen(Function3.of(this::blink).curried().apply(new HashMap<>()).apply(75))
                .apply(input);
    }

}
