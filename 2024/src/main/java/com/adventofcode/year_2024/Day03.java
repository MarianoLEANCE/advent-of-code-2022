package com.adventofcode.year_2024;

import io.vavr.Function1;
import io.vavr.Tuple2;
import io.vavr.collection.Array;
import io.vavr.collection.Traversable;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day03 {

    public int part1(String input) {
        return Function1.of(this::removeNewLines)
                .andThen(this::parse)
                .andThen(this::compute)
                .andThen(Traversable::sum)
                .andThen(Number::intValue)
                .apply(input);
    }

    private Array<Tuple2<Integer, Integer>> parse(String input) {
        Pattern pattern = Pattern.compile("mul\\((\\d+),(\\d+)\\)");
        Matcher matcher = pattern.matcher(input);
        List<Tuple2<Integer, Integer>> list = new ArrayList<>();
        while (matcher.find()) {
            list.add(new Tuple2<>(Integer.parseInt(matcher.group(1)), Integer.parseInt(matcher.group(2))));
        }

        return Array.ofAll(list);
    }

    private Array<Integer> compute(Array<Tuple2<Integer, Integer>> multiplications) {
        return multiplications.map(multiplication -> multiplication._1() * multiplication._2());
    }

    public int part2(String input) {
        return Function1.of(this::removeNewLines)
                .andThen(this::removeDonts)
                .andThen(this::parse)
                .andThen(this::compute)
                .andThen(Traversable::sum)
                .andThen(Number::intValue)
                .apply(input);
    }

    private String removeDonts(String input) {
        return input.replaceAll("don't\\(\\).*?do\\(\\)", "do()");
    }

    private String removeNewLines(String input) {
        return input.replace("\n", "");
    }

}
