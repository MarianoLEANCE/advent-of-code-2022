package com.adventofcode.year_2024;

import com.adventofcode.common.StringUtils;
import io.vavr.Function1;
import io.vavr.Tuple2;
import io.vavr.collection.Array;

public class Day02 {

    public int part1(String input) {
        return Function1.of(StringUtils.split("\n"))
                .andThen(this::reports)
                .andThen(reports -> reports.count(this::safe))
                .apply(input);
    }

    private Array<Array<Integer>> reports(Array<String> lines) {
        return lines.map(line -> Array.of(line.split(" ")).map(Integer::parseInt));
    }

    private boolean safe(Array<Integer> report) {
        Array<Tuple2<Integer, Integer>> links = report.subSequence(0, report.size() - 1)
                .zip(report.subSequence(1, report.size()));
        return (increase(links) || decrease(links)) && gradual(links);
    }

    private boolean increase(Array<Tuple2<Integer, Integer>> links) {
        return links.map(link -> link._1() < link._2()).reduce(Boolean::logicalAnd);
    }

    private boolean decrease(Array<Tuple2<Integer, Integer>> links) {
        return links.map(link -> link._1() > link._2()).reduce(Boolean::logicalAnd);
    }

    private boolean gradual(Array<Tuple2<Integer, Integer>> links) {
        return links.map(link -> Math.abs(link._1() - link._2()) <= 3).reduce(Boolean::logicalAnd);
    }

    public int part2(String input) {
        return Function1.of(StringUtils.split("\n"))
                .andThen(this::reports)
                .andThen(reports -> reports.count(this::safeWithDampener))
                .apply(input);
    }

    private boolean safeWithDampener(Array<Integer> report) {
        return safe(report) ||
               // apply dampener!
               Array.range(0, report.size()).map(report::removeAt)
                       .map(this::safe)
                       .reduce(Boolean::logicalOr);
    }

}
