package com.adventofcode.year_2024;

import com.adventofcode.common.Grid;
import io.vavr.Function1;
import io.vavr.Tuple2;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Day06 {

    public int part1(String input) {
        return Function1.of(this::parse)
                .andThen(this::walk)
                .andThen(grid -> grid.count('X'))
                .apply(input);
    }

    public int part2(String input) {
        return (int) addObstacles(input)
                .map(this::parse)
                .map(this::walk)
                .filter(this::isLoop)
                .count();
    }

    private Stream<String> addObstacles(String input) {
        return IntStream.range(0, input.length())
                .parallel()
                .filter(i -> input.charAt(i) == '.')
                .mapToObj(i -> input.substring(0, i) + '#' + input.substring(i + 1));
    }

    private Day06Room parse(String input) {
        String[] splitted = input.split("\n");
        Day06Room grid = new Day06Room(splitted.length, splitted[0].length());
        for (int row = 0; row < splitted.length; row++) {
            for (int col = 0; col < splitted[row].length(); col++) {
                grid.set(row, col, splitted[row].charAt(col));
            }
        }
        grid.find('^').ifPresent(grid::setGuard);
        return grid;
    }

    int count = 0;

    private Day06Room walk(Day06Room grid) {
        while (grid.step()) {}
        count++;
        return grid;
    }

    private boolean isLoop(Grid<Character> grid) {
        return !grid.find(cell -> List.of('^', '>', 'v', '<').contains(cell.getValue())).isEmpty();
    }

    @Getter
    @Setter
    private class Day06Room extends Grid<Character> {

        private Cell<Character> guard;
        private boolean loop = false;
        private List<Tuple2<Cell<Character>, Character>> traversed = new ArrayList<>();

        public Day06Room(int rows, int columns) {
            super(rows, columns);
        }

        public boolean step() {
            return switch (guard.getValue()) {
                case '^' -> step(guard::getUp, '>');
                case '>' -> step(guard::getRight, 'v');
                case 'v' -> step(guard::getDown, '<');
                case '<' -> step(guard::getLeft, '^');
                default -> false;
            };
        }

        private boolean step(Supplier<Cell<Character>> forward, char turn) {
            Cell<Character> nextCell = forward.get();
            if (nextCell == null) {
                guard.setValue('X');
                return false;
            }
            if (nextCell.getValue() != '#') {
                nextCell.setValue(guard.getValue());
                guard.setValue('X');
                guard = nextCell;
                if (traversed.contains(new Tuple2<>(guard, guard.getValue()))) {
                    loop = true;
                } else {
                    traversed.add(new Tuple2<>(guard, guard.getValue()));
                }
            } else {
                guard.setValue(turn);
            }
            return !loop;
        }

    }

}
