package com.adventofcode.year_2024;

import io.vavr.Function1;
import io.vavr.Function2;
import io.vavr.Tuple2;
import io.vavr.collection.Array;
import io.vavr.collection.Traversable;

public class Day07 {

    public long part1(String input) {
        return Function1.of(this::equations)
                .andThen(equations -> equations.filter(eq -> solvable(operators1(), eq)))
                .andThen(equations -> equations.map(Tuple2::_1))
                .andThen(Traversable::sum)
                .andThen(Number::longValue)
                .apply(input);
    }

    private Array<Tuple2<Long, Array<Long>>> equations(String input) {
        return Array.of(input.split("\n"))
                .map(line -> {
                    String[] parts = line.split(": ");
                    Array<Long> values = Array.of(parts[1].split(" ")).map(Long::parseLong);
                    return new Tuple2<>(Long.parseLong(parts[0]), values);
                });
    }

    private Array<Function2<Long, Long, Long>> operators1() {
        return Array.of(Long::sum, (x, y) -> x * y);
    }

    private boolean solvable(Array<Function2<Long, Long, Long>> operators, Tuple2<Long, Array<Long>> equation) {
        long level = equation._1();
        Array<Long> values = equation._2();
        Array<Long> possibleValues = compute(Array.of(values.get()), values.subSequence(1), operators)._1();
        return possibleValues.contains(level);
    }

    private Tuple2<Array<Long>, Array<Long>> compute(Array<Long> partials, Array<Long> values,
                                                     Array<Function2<Long, Long, Long>> operators) {
        if (values.isEmpty()) {
            return new Tuple2<>(partials, values);
        }
        Long next = values.head();
        Array<Function1<Long, Long>> curried = operators.map(op -> op.apply(next));
        Array<Long> nextVals = curried.map(partials::map).reduce(Array::appendAll);
        return compute(nextVals, values.subSequence(1), operators);
    }

    public long part2(String input) {
        return Function1.of(this::equations)
                .andThen(equations -> equations.filter(eq -> solvable(operators2(), eq)))
                .andThen(equations -> equations.map(Tuple2::_1))
                .andThen(Traversable::sum)
                .andThen(Number::longValue)
                .apply(input);
    }

    private Array<Function2<Long, Long, Long>> operators2() {
        return Array.of(
                Long::sum,
                (x, y) -> x * y,
                (x, y) -> Long.parseLong(Long.toString(y).concat(Long.toString(x)))
        );
    }

}
