package com.adventofcode.year_2024;

import io.vavr.Function1;
import io.vavr.collection.Array;

public class Day04 {

    public int part1(String input) {
        return Function1.of(this::grid)
                .andThen(this::scanXMAS)
                .apply(input);
    }

    private char[][] grid(String input) {
        return Array.of(input.split("\n")).map(String::toCharArray).toJavaArray(char[][]::new);
    }

    private int scanXMAS(char[][] grid) {
        int count = 0;
        for (int row = 0; row < grid.length; row++) {
            for (int col = 0; col < grid[row].length; col++) {
                if (scanLeft(grid, row, col)) {
                    count++;
                }
                if (scanDownLeft(grid, row, col)) {
                    count++;
                }
                if (scanDown(grid, row, col)) {
                    count++;
                }
                if (scanDownRight(grid, row, col)) {
                    count++;
                }
                if (scanRight(grid, row, col)) {
                    count++;
                }
                if (scanUpRight(grid, row, col)) {
                    count++;
                }
                if (scanUp(grid, row, col)) {
                    count++;
                }
                if (scanUpLeft(grid, row, col)) {
                    count++;
                }
            }
        }
        return count;
    }

    private boolean scanLeft(char[][] grid, int row, int col) {
        if (col - 3 < 0) {
            return false;
        }
        return grid[row][col] == 'X' &&
               grid[row][col - 1] == 'M' &&
               grid[row][col - 2] == 'A' &&
               grid[row][col - 3] == 'S';
    }

    private boolean scanDownLeft(char[][] grid, int row, int col) {
        if (row + 3 >= grid.length || col - 3 < 0) {
            return false;
        }
        return grid[row][col] == 'X' &&
               grid[row + 1][col - 1] == 'M' &&
               grid[row + 2][col - 2] == 'A' &&
               grid[row + 3][col - 3] == 'S';
    }

    private boolean scanDown(char[][] grid, int row, int col) {
        if (row + 3 >= grid.length) {
            return false;
        }
        return grid[row][col] == 'X' &&
               grid[row + 1][col] == 'M' &&
               grid[row + 2][col] == 'A' &&
               grid[row + 3][col] == 'S';
    }

    private boolean scanDownRight(char[][] grid, int row, int col) {
        if (row + 3 >= grid.length || col + 3 >= grid[0].length) {
            return false;
        }
        return grid[row][col] == 'X' &&
               grid[row + 1][col + 1] == 'M' &&
               grid[row + 2][col + 2] == 'A' &&
               grid[row + 3][col + 3] == 'S';
    }

    private boolean scanRight(char[][] grid, int row, int col) {
        if (col + 3 >= grid[0].length) {
            return false;
        }
        return grid[row][col] == 'X' &&
               grid[row][col + 1] == 'M' &&
               grid[row][col + 2] == 'A' &&
               grid[row][col + 3] == 'S';
    }

    private boolean scanUpRight(char[][] grid, int row, int col) {
        if (row - 3 < 0 || col + 3 >= grid[0].length) {
            return false;
        }
        return grid[row][col] == 'X' &&
               grid[row - 1][col + 1] == 'M' &&
               grid[row - 2][col + 2] == 'A' &&
               grid[row - 3][col + 3] == 'S';
    }

    private boolean scanUp(char[][] grid, int row, int col) {
        if (row - 3 < 0) {
            return false;
        }
        return grid[row][col] == 'X' &&
               grid[row - 1][col] == 'M' &&
               grid[row - 2][col] == 'A' &&
               grid[row - 3][col] == 'S';
    }

    private boolean scanUpLeft(char[][] grid, int row, int col) {
        if (row - 3 < 0 || col - 3 < 0) {
            return false;
        }
        return grid[row][col] == 'X' &&
               grid[row - 1][col - 1] == 'M' &&
               grid[row - 2][col - 2] == 'A' &&
               grid[row - 3][col - 3] == 'S';
    }

    public int part2(String input) {
        return Function1.of(this::grid)
                .andThen(this::scanCrossMAS)
                .apply(input);
    }

    private int scanCrossMAS(char[][] grid) {
        int count = 0;
        for (int row = 1; row < grid.length - 1; row++) {
            for (int col = 1; col < grid[row].length - 1; col++) {
                boolean centeredA = grid[row][col] == 'A';

                char ul = grid[row - 1][col - 1];
                char ur = grid[row - 1][col + 1];
                char dl = grid[row + 1][col - 1];
                char dr = grid[row + 1][col + 1];

                boolean diag1 = (ul == 'M' && dr == 'S') || (ul == 'S' && dr == 'M');
                boolean diag2 = (ur == 'M' && dl == 'S') || (ur == 'S' && dl == 'M');
                if (centeredA && diag1 && diag2) {
                    count++;
                }
            }
        }
        return count;
    }

}
