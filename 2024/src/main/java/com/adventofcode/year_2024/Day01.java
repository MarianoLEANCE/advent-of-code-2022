package com.adventofcode.year_2024;

import io.vavr.Function1;
import io.vavr.Tuple2;
import io.vavr.collection.Array;

public class Day01 {

    public int part1(String input) {
        return Function1.<String, String[]>of(x -> x.split("\n"))
                .andThen(Array::of)
                .andThen(this::separateNotes)
                .andThen(this::sortNotes)
                .andThen(this::distance)
                .apply(input);
    }

    private Array<Tuple2<String, String>> separateNotes(Array<String> notes) {
        return notes.map(this::separateNotes);
    }

    private Tuple2<String, String> separateNotes(String notes) {
        String firstGroup = notes.substring(0, notes.indexOf(" "));
        String secondGroup = notes.substring(notes.lastIndexOf(" ") + 1);
        return new Tuple2<>(firstGroup, secondGroup);
    }

    private Array<Tuple2<Integer, Integer>> sortNotes(Array<Tuple2<String, String>> notes) {
        Array<Integer> group1Sorted = notes.map(Tuple2::_1).map(Integer::parseInt).sorted();
        Array<Integer> group2Sorted = notes.map(Tuple2::_2).map(Integer::parseInt).sorted();
        return Array.range(0, notes.size())
                .map(i -> new Tuple2<>(group1Sorted.get(i), group2Sorted.get(i)));
    }

    private Integer distance(Array<Tuple2<Integer, Integer>> notes) {
        return notes.map(x -> Math.abs(x._1() - x._2())).reduce(Integer::sum);
    }

    public int part2(String input) {
        return Function1.<String, String[]>of(x -> x.split("\n"))
                .andThen(Array::of)
                .andThen(this::separateNotes)
                .andThen(this::groupNotes)
                .andThen(this::similarity)
                .apply(input);
    }

    private Tuple2<Array<Integer>, Array<Integer>> groupNotes(Array<Tuple2<String, String>> notes) {
        Array<Integer> group1Sorted = notes.map(Tuple2::_1).map(Integer::parseInt).sorted();
        Array<Integer> group2Sorted = notes.map(Tuple2::_2).map(Integer::parseInt).sorted();
        return new Tuple2<>(group1Sorted, group2Sorted);
    }

    private Integer similarity(Tuple2<Array<Integer>, Array<Integer>> notes) {
        Array<Integer> group1 = notes._1();
        Array<Integer> group2 = notes._2();
        return group1.map(i -> i * group2.count(i::equals)).sum().intValue();
    }

//    private int calibrationValue(String s) {
//        Array<Character> digits = Array.ofAll(s.toCharArray()).filter(Character::isDigit);
//        String first = digits.head().toString();
//        String last = digits.last().toString();
//        return Integer.parseInt(first + last);
//    }
//
//    private int completeCalibrationValue(String s) {
//        Set<String> numbers = TreeSet.of("1", "2", "3", "4", "5", "6", "7", "8", "9",
//                "one", "two", "three", "four", "five", "six", "seven", "eight", "nine");
//
//        String first = numbers.filter(s::contains)
//                .toSortedMap(number -> new Tuple2<>(s.indexOf(number), number))
//                .head()
//                .map(x -> x, this::replace)
//                ._2();
//        String last = numbers.filter(s::contains)
//                .toSortedMap(number -> new Tuple2<>(s.lastIndexOf(number), number))
//                .last()
//                .map(x -> x, this::replace)
//                ._2();
//        return Integer.parseInt(first + last);
//    }
//
//    private String replace(String s) {
//        return s.replace("one", "1")
//                .replace("two", "2")
//                .replace("three", "3")
//                .replace("four", "4")
//                .replace("five", "5")
//                .replace("six", "6")
//                .replace("seven", "7")
//                .replace("eight", "8")
//                .replace("nine", "9");
//    }

}
