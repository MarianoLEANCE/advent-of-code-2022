package com.adventofcode.year_2024;

import io.vavr.Function1;
import io.vavr.collection.Array;
import io.vavr.collection.Traversable;

public class Day05 {

    public int part1(String input) {
        return Function1.of(this::parse)
                .andThen(this::filterCorrectUpdates)
                .andThen(game -> game.updates().map(this::middlePageNumber))
                .andThen(Traversable::sum)
                .andThen(Number::intValue)
                .apply(input);
    }

    private Game parse(String input) {
        String[] parts = input.split("\n\n");
        return new Game(parseRules(parts[0]), parseUpdates(parts[1]));
    }

    private Array<PageOrderingRule> parseRules(String rules) {
        return Array.of(rules.split("\n")).map(rule -> {
            String[] pages = rule.split("\\|");
            int firstPage = Integer.parseInt(pages[0]);
            int secondPage = Integer.parseInt(pages[1]);
            return new PageOrderingRule(firstPage, secondPage);
        });
    }

    private Array<Array<Integer>> parseUpdates(String input) {
        return Array.of(input.split("\n"))
                .map(line -> Array.of(line.split(",")).map(Integer::valueOf));
    }

    private Game filterCorrectUpdates(Game game) {
        Array<Array<Integer>> correctUpdates = game.updates.filter(update -> game.rules
                .map(rule -> rule.valid(update))
                .reduce(Boolean::logicalAnd));
        return new Game(game.rules, correctUpdates);
    }

    private int middlePageNumber(Array<Integer> update) {
        return update.get(update.size() / 2);
    }

    private record Game(Array<PageOrderingRule> rules, Array<Array<Integer>> updates) {}

    private record PageOrderingRule(int firstPage, int secondPage) {

        public boolean valid(Array<Integer> update) {
            if (!update.contains(firstPage)) {
                return true;
            }
            int indexOfFirstPage = update.indexOf(firstPage);
            return !update.subSequence(0, indexOfFirstPage).contains(secondPage);
        }

    }

    public int part2(String input) {
        return Function1.of(this::parse)
                .andThen(this::filterIncorrectUpdates)
                .andThen(this::fixUpates)
                .andThen(game -> game.updates().map(this::middlePageNumber))
                .andThen(Traversable::sum)
                .andThen(Number::intValue)
                .apply(input);
    }

    private Game filterIncorrectUpdates(Game game) {
        Array<Array<Integer>> incorrectUpdates = game.updates
                .filter(update -> game.rules.map(rule -> !rule.valid(update)).reduce(Boolean::logicalOr));
        return new Game(game.rules, incorrectUpdates);
    }

    private Game fixUpates(Game game) {
        Array<Array<Integer>> updates = game.updates();
        return new Game(game.rules, updates.map(x -> fixUpdate(game.rules, x)));
    }

    private Array<Integer> fixUpdate(Array<PageOrderingRule> rules, Array<Integer> update) {
        for (PageOrderingRule rule : rules) {
            if (!rule.valid(update)) {
                int secondPageIndex = update.indexOf(rule.secondPage);
                int firstPageIndex = update.indexOf(rule.firstPage);
                Array<Integer> swap = update
                        .removeAt(firstPageIndex)
                        .removeAt(secondPageIndex).insert(secondPageIndex, rule.firstPage)
                        .insert(firstPageIndex, rule.secondPage);
                return fixUpdate(rules, swap);
            }
        }
        return update;
    }

}
