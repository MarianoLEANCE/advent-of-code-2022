package com.adventofcode.year_2024;

import com.adventofcode.common.InputUtils;
import io.vavr.control.Try;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day09Test {

    Day09 day = new Day09();

    @Test
    void part1Example() {
        long expected = day.part1(example());
        assertEquals(1928, expected);
    }

    @Test
    void part1() {
        long expected = input().map(day::part1).get();
        assertEquals(6398252054886L, expected);
    }

//    @Test
//    void part2Example() {
//        long expected = day.part2(example());
//        assertEquals(2858, expected);
//    }

//    @Test
//    void part2() {
//        long expected = input().map(day::part2).get();
//        assertEquals(124060392153684L, expected);
//    }

    private Try<String> input() {
        return InputUtils.readFromClassPath("day09.txt");
    }

    private String example() {
        return "2333133121414131402";
    }

}
