package com.adventofcode.year_2024;

import com.adventofcode.common.InputUtils;
import io.vavr.control.Try;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day07Test {

    Day07 day = new Day07();

    @Test
    void part1Example() {
        long expected = day.part1(example());
        assertEquals(3749, expected);
    }

    @Test
    void part1() {
        long expected = input().map(day::part1).get();
        assertEquals(2654749936343L, expected);
    }

    @Test
    void part2Example() {
        long expected = day.part2(example());
        assertEquals(11387, expected);
    }

    @Test
    void part2() {
        long expected = input().map(day::part2).get();
        assertEquals(124060392153684L, expected);
    }

    private Try<String> input() {
        return InputUtils.readFromClassPath("day07.txt");
    }

    private String example() {
        return """
                190: 10 19
                3267: 81 40 27
                83: 17 5
                156: 15 6
                7290: 6 8 6 15
                161011: 16 10 13
                192: 17 8 14
                21037: 9 7 18 13
                292: 11 6 16 20
                """;
    }

}
