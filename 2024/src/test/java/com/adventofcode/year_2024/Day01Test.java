package com.adventofcode.year_2024;

import com.adventofcode.common.InputUtils;
import io.vavr.control.Try;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day01Test {

    Day01 day = new Day01();

    @Test
    void part1Example() {
        int expected = day.part1(example());
        assertEquals(11, expected);
    }

    @Test
    void part1() {
        int expected = input().map(day::part1).get();
        assertEquals(2769675, expected);
    }

    @Test
    void part2Example() {
        int expected = day.part2(example());
        assertEquals(31, expected);
    }

    @Test
    void part2() {
        int expected = input().map(day::part2).get();
        assertEquals(24643097, expected);
    }

    private String example() {
        return """
                3   4
                4   3
                2   5
                1   3
                3   9
                3   3
                """;
    }

    private Try<String> input() {
        return InputUtils.readFromClassPath("day01.txt");
    }

}
