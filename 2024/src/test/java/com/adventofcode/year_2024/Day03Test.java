package com.adventofcode.year_2024;

import com.adventofcode.common.InputUtils;
import io.vavr.control.Try;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day03Test {

    Day03 day = new Day03();

    @Test
    void part1Example() {
        int expected = day.part1("xmul(2,4)%&mul[3,7]!@^do_not_mul(5,5)+mul(32,64]then(mul(11,8)mul(8,5))");
        assertEquals(161, expected);
    }

    @Test
    void part1() {
        int expected = input().map(day::part1).get();
        assertEquals(174336360, expected);
    }

    @Test
    void part2Example() {
        int expected = day.part2("xmul(2,4)&mul[3,7]!^don't()_mul(5,5)+mul(32,64](mul(11,8)undo()?mul(8,5))");
        assertEquals(48, expected);
    }

    @Test
    void part2() {
        int expected = input().map(day::part2).get();
        assertEquals(88802350, expected);
    }

    private Try<String> input() {
        return InputUtils.readFromClassPath("day03.txt");
    }

}
