package com.adventofcode.year_2024;

import com.adventofcode.common.InputUtils;
import io.vavr.control.Try;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day11Test {

    Day11 day = new Day11();

    @Test
    void part1Example() {
        long expected = day.part1(example());
        assertEquals(55312L, expected);
    }

    @Test
    void part1() {
        long expected = input().map(day::part1).get();
        assertEquals(239714L, expected);
    }

    @Test
    void part2() {
        long expected = input().map(day::part2).get();
        assertEquals(284973560658514L, expected);
    }

    private Try<String> input() {
        return InputUtils.readFromClassPath("day11.txt");
    }

    private String example() {
        return "125 17";
    }

}
