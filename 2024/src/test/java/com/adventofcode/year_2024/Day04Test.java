package com.adventofcode.year_2024;

import com.adventofcode.common.InputUtils;
import io.vavr.control.Try;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day04Test {

    Day04 day = new Day04();

    @Test
    void part1Example() {
        int expected = day.part1("""
                MMMSXXMASM
                MSAMXMSMSA
                AMXSXMAAMM
                MSAMASMSMX
                XMASAMXAMM
                XXAMMXXAMA
                SMSMSASXSS
                SAXAMASAAA
                MAMMMXMMMM
                MXMXAXMASX
                """);
        assertEquals(18, expected);
    }

    @Test
    void part1() {
        int expected = input().map(day::part1).get();
        assertEquals(2644, expected);
    }

    @Test
    void part2Example() {
        int expected = day.part2("""
                MMMSXXMASM
                MSAMXMSMSA
                AMXSXMAAMM
                MSAMASMSMX
                XMASAMXAMM
                XXAMMXXAMA
                SMSMSASXSS
                SAXAMASAAA
                MAMMMXMMMM
                MXMXAXMASX
                """);
        assertEquals(9, expected);
    }

    @Test
    void part2() {
        int expected = input().map(day::part2).get();
        assertEquals(1952, expected);
    }

    private Try<String> input() {
        return InputUtils.readFromClassPath("day04.txt");
    }

}
