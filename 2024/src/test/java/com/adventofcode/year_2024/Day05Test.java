package com.adventofcode.year_2024;

import com.adventofcode.common.InputUtils;
import io.vavr.control.Try;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day05Test {

    Day05 day = new Day05();

    @Test
    void part1Example() {
        int expected = day.part1(example());
        assertEquals(143, expected);
    }

    @Test
    void part1() {
        int expected = input().map(day::part1).get();
        assertEquals(4996, expected);
    }

    @Test
    void part2Example() {
        int expected = day.part2(example());
        assertEquals(123, expected);
    }

    @Test
    void part2() {
        int expected = input().map(day::part2).get();
        assertEquals(6311, expected);
    }

    private Try<String> input() {
        return InputUtils.readFromClassPath("day05.txt");
    }

    private String example() {
        return """
                47|53
                97|13
                97|61
                97|47
                75|29
                61|13
                75|53
                29|13
                97|29
                53|29
                61|53
                97|53
                61|29
                47|13
                75|47
                97|75
                47|61
                75|61
                47|29
                75|13
                53|13
                
                75,47,61,53,29
                97,61,53,29,13
                75,29,13
                75,97,47,61,53
                61,13,29
                97,13,75,29,47
                """;
    }

}
