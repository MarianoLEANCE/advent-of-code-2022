package com.adventofcode.year_2024;

import com.adventofcode.common.InputUtils;
import io.vavr.control.Try;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day02Test {

    Day02 day = new Day02();

    @Test
    void part1Example() {
        int expected = day.part1(example());
        assertEquals(2, expected);
    }

    @Test
    void part1() {
        int expected = input().map(day::part1).get();
        assertEquals(631, expected);
    }

    @Test
    void part2Example() {
        int expected = day.part2(example());
        assertEquals(4, expected);
    }

    @Test
    void part2() {
        int expected = input().map(day::part2).get();
        assertEquals(665, expected);
    }

    private String example() {
        return """
                7 6 4 2 1
                1 2 7 8 9
                9 7 6 2 1
                1 3 2 4 5
                8 6 4 4 1
                1 3 6 7 9
                """;
    }

    private Try<String> input() {
        return InputUtils.readFromClassPath("day02.txt");
    }

}
