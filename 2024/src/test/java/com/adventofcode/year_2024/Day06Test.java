package com.adventofcode.year_2024;

import com.adventofcode.common.InputUtils;
import io.vavr.control.Try;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day06Test {

    Day06 day = new Day06();

    @Test
    void part1Example() {
        int expected = day.part1(example());
        assertEquals(41, expected);
    }

    @Test
    void part1() {
        int expected = input().map(day::part1).get();
        assertEquals(4982, expected);
    }

    @Test
    void part2Example() {
        int expected = day.part2(example());
        assertEquals(6, expected);
    }
// todo slow
//    @Test
//    void part2() {
//        int expected = input().map(day::part2).get();
//        assertEquals(1663, expected);
//    }

    private Try<String> input() {
        return InputUtils.readFromClassPath("day06.txt");
    }

    private String example() {
        return """
                ....#.....
                .........#
                ..........
                ..#.......
                .......#..
                ..........
                .#..^.....
                ........#.
                #.........
                ......#...
                """;
    }

}
