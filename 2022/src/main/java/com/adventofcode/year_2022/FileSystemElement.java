package com.adventofcode.year_2022;

public interface FileSystemElement {

    String name();

    int size();

}
