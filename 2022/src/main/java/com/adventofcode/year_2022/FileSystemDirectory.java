package com.adventofcode.year_2022;

import java.util.ArrayList;
import java.util.Collection;

public class FileSystemDirectory implements FileSystemElement {

    private final FileSystemDirectory parent;

    private final String name;
    private final Collection<FileSystemElement> elements = new ArrayList<>();

    public FileSystemDirectory(FileSystemDirectory parent, String name) {
        this.parent = parent;
        this.name = name;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public int size() {
        return elements.stream().mapToInt(FileSystemElement::size).sum();
    }

    public void createDirectory(String name) {
        elements.add(new FileSystemDirectory(this, name));
    }

    public void createFile(String name, int size) {
        elements.add(new FileSystemFile(name, size));
    }

    public FileSystemDirectory parent() {
        return parent;
    }

    public FileSystemDirectory cd(String name) {
        return elements.stream().filter(element -> element.name().equals(name))
                .filter(FileSystemDirectory.class::isInstance)
                .map(FileSystemDirectory.class::cast)
                .findAny().get();
    }

    public Collection<FileSystemDirectory> lsdir() {
        return elements.stream()
                .filter(FileSystemDirectory.class::isInstance)
                .map(FileSystemDirectory.class::cast)
                .toList();
    }

}
