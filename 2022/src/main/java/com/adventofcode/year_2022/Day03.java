package com.adventofcode.year_2022;

import io.vavr.Function1;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import io.vavr.collection.Traversable;
import lombok.Getter;

/**
 * @see <a href="https://adventofcode.com/2022/day/3">Day 3</a>
 */
public class Day03 {

    public static Integer part1(String input) {
        return Function1.of(Day03::rucksacks)
                .andThen(rucksacks -> rucksacks.map(Day03::compartments))
                .andThen(x -> x.map(Day03::intersect))
                .andThen(x -> x.map(Traversable::get))
                .andThen(x -> x.map(Day03::priority))
                .andThen(Traversable::sum)
                .apply(input)
                .intValue();
    }

    public static Integer part2(String input) {
        return Function1.of(Day03::rucksacks)
                .andThen(Day03::teams)
                .andThen(teams -> teams.map(Day03::badge))
                .andThen(badges -> badges.map(Day03::priority))
                .andThen(Traversable::sum)
                .apply(input)
                .intValue();
    }

    private static List<Rucksack> rucksacks(String input) {
        return List.of(input.split("\n")).map(Rucksack::new);
    }

    private static List<Set<Integer>> compartments(Rucksack rucksack) {
        return List.of(rucksack.leftCompartment().toSet(), rucksack.rightCompartment().toSet());
    }

    private static Set<Integer> intersect(List<Set<Integer>> itemSets) {
        if (itemSets.size() == 2) {
            return itemSets.get(0).intersect(itemSets.get(1));
        }
        return itemSets.get(0).intersect(intersect(itemSets.subSequence(1)));
    }

    private static int priority(int item) {
        if ('a' <= item && item <= 'z') {
            return item - 'a' + 1;
        }
        if ('A' <= item && item <= 'Z') {
            return item - 'A' + 27;
        }
        throw new IllegalArgumentException();
    }

    private static List<List<Rucksack>> teams(List<Rucksack> rucksacks) {
        if (rucksacks.isEmpty()) {
            return List.empty();
        }
        List<Rucksack> nextTeam = rucksacks.take(3);
        List<Rucksack> otherElves = rucksacks.drop(3);
        List<List<Rucksack>> otherTeams = teams(otherElves);
        return List.of(nextTeam).appendAll(otherTeams);
    }

    private static int badge(List<Rucksack> team) {
        Set<Integer> a = team.get(0).getItems().toSet();
        Set<Integer> b = team.get(1).getItems().toSet();
        Set<Integer> c = team.get(2).getItems().toSet();
        Set<Integer> intersection = intersect(List.of(a, b, c));
        return intersection.get();
    }

    @Getter
    public static class Rucksack {

        private final List<Integer> items;

        public Rucksack(String s) {
            this.items = List.ofAll(s.chars().boxed().toList());
        }

        public List<Integer> leftCompartment() {
            return items.subSequence(0, items.size() / 2);
        }

        public List<Integer> rightCompartment() {
            return items.subSequence(items.length() / 2);
        }

    }

}
