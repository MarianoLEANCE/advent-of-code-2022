package com.adventofcode.year_2022;

import io.vavr.Function1;

import java.util.List;
import java.util.stream.Stream;

/**
 * @see <a href="https://adventofcode.com/2022/day/10">Day 10</a>
 */
public class Day10 {

    public static Integer part1(String input) {
        return Function1.of(Day10::parse)
                .andThen(Day10::strength)
                .apply(input);
    }

    public static String part2(String input) {
        return Function1.of(Day10::parse)
                .andThen(State::print)
                .apply(input);
    }

    private static int strength(State state) {
        return Stream.of(20, 60, 100, 140, 180, 220)
                .map(state::get)
                .mapToInt(State::strength)
                .sum();
    }

    private static State parse(String input) {
        String[] lines = input.split("\n");
        State start = new State(1, 0, null);
        List<Instruction> instructions = Stream.of(lines).map(line -> {
            if (line.equals("noop")) {
                return new Noop();
            } else {
                return new Addx(Integer.parseInt(line.substring(5)));
            }
        }).toList();
        return execute(start, instructions);
    }

    private static State execute(State state, List<Instruction> instructions) {
        if (instructions.isEmpty()) {
            return state;
        }
        return execute(instructions.get(0).execute(state), instructions.subList(1, instructions.size()));
    }

    record State(int register, int cycle, State previous) {

        State get(int index) {
            if (cycle == index) {
                return this;
            } else {
                return previous.get(index);
            }
        }

        int strength() {
            return previous.register * cycle;
        }

        @Override
        public String toString() {
            return "cycle=" + cycle + ", register=" + register;
        }

        public String print() {
            StringBuilder builder = new StringBuilder();
            if (previous != null) {
                builder.append(previous.print());
            }
            if (cycle % 40 == 0) {
                builder.append("\n");
            }
            builder.append(isPixelLit() ? "#" : ".");
            return builder.toString();
        }

        public boolean isPixelLit() {
            int position = cycle % 40;
            return position == register - 1 || position == register || position == register + 1;
        }

    }

    interface Instruction {

        State execute(State state);

    }

    static class Noop implements Instruction {

        @Override
        public State execute(State state) {
            return new State(state.register(), state.cycle() + 1, state);
        }

        @Override
        public String toString() {
            return "noop";
        }

    }

    static class Addx implements Instruction {

        private final int x;

        public Addx(int x) {this.x = x;}

        @Override
        public State execute(State state) {
            State s1 = new State(state.register(), state.cycle() + 1, state);
            return new State(s1.register() + x, s1.cycle() + 1, s1);
        }

        @Override
        public String toString() {
            return "addx " + x;
        }

    }

}
