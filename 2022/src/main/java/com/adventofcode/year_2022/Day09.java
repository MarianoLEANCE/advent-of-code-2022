package com.adventofcode.year_2022;

import lombok.Getter;
import lombok.ToString;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * todo refactor to pure code
 */
public class Day09 {

    public static Integer part1(String input) {
        String[] lines = input.split("\n");
        Pattern p = Pattern.compile("([URDL]) (\\d+)");

        Knot tail = new Knot(null);
        Knot head = new Knot(tail);

        Stream.of(lines).forEach(line -> {
            Matcher m = p.matcher(line);
            if (m.matches()) {
                char direction = m.group(1).charAt(0);
                int distance = Integer.parseInt(m.group(2));
                head.move(direction, distance);
            }
        });

        return tail.positions.size();
    }

    public static Integer part2(String input) {
        String[] lines = input.split("\n");
        Pattern p = Pattern.compile("([URDL]) (\\d+)");

        Knot k9 = new Knot(null);
        Knot k8 = new Knot(k9);
        Knot k7 = new Knot(k8);
        Knot k6 = new Knot(k7);
        Knot k5 = new Knot(k6);
        Knot k4 = new Knot(k5);
        Knot k3 = new Knot(k4);
        Knot k2 = new Knot(k3);
        Knot k1 = new Knot(k2);
        Knot head = new Knot(k1);

        Stream.of(lines).forEach(line -> {
            Matcher m = p.matcher(line);
            if (m.matches()) {
                char direction = m.group(1).charAt(0);
                int distance = Integer.parseInt(m.group(2));
                head.move(direction, distance);
            }
        });

        return k9.positions.size();
    }

    @Getter
    @ToString(of = {"x", "y"})
    static class Knot {

        private int x = 0;
        private int y = 0;
        private final Set<Pair<Integer, Integer>> positions = new HashSet<>();
        private final Knot next;

        Knot(Knot next) {
            this.next = next;
            this.trace();
        }

        void move(char direction, int amount) {
            IntStream.rangeClosed(1, amount).forEach(i -> move(direction));
        }

        void move(char direction) {
            switch (direction) {
                case 'U' -> y++;
                case 'R' -> x++;
                case 'D' -> y--;
                case 'L' -> x--;
            }
            if (next != null) {
                next.follow(this);
            }
        }

        void follow(Knot previous) {
            if (previous.y - y == 2) {
                y++;
                if (previous.x - x == 2) {
                    x++;
                } else if (previous.x - x == -2) {
                    x--;
                } else {
                    x = previous.x;
                }
                if (next != null) {
                    next.follow(this);
                }
            } else if (previous.y - y == -2) {
                y--;
                if (previous.x - x == 2) {
                    x++;
                } else if (previous.x - x == -2) {
                    x--;
                } else {
                    x = previous.x;
                }
                if (next != null) {
                    next.follow(this);
                }
            } else if (previous.x - x == 2) {
                x++;
                if (previous.y - y == 2) {
                    y++;
                } else if (previous.y - y == -2) {
                    y--;
                } else {
                    y = previous.y;
                }
                if (next != null) {
                    next.follow(this);
                }
            } else if (previous.x - x == -2) {
                x--;
                if (previous.y - y == 2) {
                    y++;
                } else if (previous.y - y == -2) {
                    y--;
                } else {
                    y = previous.y;
                }
                if (next != null) {
                    next.follow(this);
                }
            }
            this.trace();
        }

        void trace() {
            positions.add(new Pair<>(x, y));
        }

    }

}
