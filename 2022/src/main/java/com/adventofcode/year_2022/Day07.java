package com.adventofcode.year_2022;

import io.vavr.Function1;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * todo refactor to pure code
 */
public class Day07 {

    public static Integer part1(String input) {
        return Function1.of(Day07::parse)
                .andThen(root -> {
                    Set<FileSystemDirectory> directories = new HashSet<>();
                    findDirs(directories, root);
                    return directories.stream().mapToInt(FileSystemDirectory::size).sum();
                })
                .apply(input);
    }

    public static Integer part2(String input) {
        return Function1.of(Day07::parse)
                .andThen(root -> {
                    Set<FileSystemDirectory> directories = new HashSet<>();
                    findAllDirs(directories, root);
                    int available = 70000000 - root.size();
                    int needed = 30000000 - available;
                    return directories.stream()
                            .mapToInt(FileSystemDirectory::size)
                            .filter(size -> size >= needed)
                            .min().orElse(0);
                })
                .apply(input);
    }

    private static FileSystemDirectory parse(String input) {
        FileSystemDirectory root = new FileSystemDirectory(null, "/");
        FileSystemDirectory cdir = root;
        Iterator<String> it = List.of(input.split("\n")).iterator();

        String line = it.next();
        do {
            if ("$ cd /".equals(line)) {
                cdir = root;
                line = it.next();
            } else if (line.matches("\\$ cd \\.\\.")) {
                cdir = cdir.parent();
                line = it.next();
            } else if (line.matches("\\$ cd (.+)")) {
                Pattern p = Pattern.compile("\\$ cd (.+)");
                Matcher matcher = p.matcher(line);
                if (matcher.find()) {
                    String name = matcher.group(1);
                    cdir = cdir.cd(name);
                    line = it.next();
                }
            } else if ("$ ls".equals(line)) {
                line = it.next();
                do {
                    if (line.matches("(\\d+) (.+)")) {
                        Pattern p = Pattern.compile("(\\d+) (.+)");
                        Matcher matcher = p.matcher(line);
                        if (matcher.find()) {
                            int size = Integer.parseInt(matcher.group(1));
                            String name = matcher.group(2);
                            cdir.createFile(name, size);
                            if (it.hasNext()) {
                                line = it.next();
                            }
                        }
                    } else if (line.matches("dir (.+)")) {
                        Pattern p = Pattern.compile("dir (.+)");
                        Matcher matcher = p.matcher(line);
                        if (matcher.find()) {
                            String name = matcher.group(1);
                            cdir.createDirectory(name);
                            line = it.next();
                        }
                    }
                } while (!line.startsWith("$") && it.hasNext());
            }
        } while (!line.isBlank() && it.hasNext());
        return root;
    }

    private static void findDirs(Set<FileSystemDirectory> directories, FileSystemDirectory cdir) {
        if (cdir.size() < 100000) {
            directories.add(cdir);
        }
        cdir.lsdir().forEach(dir -> findDirs(directories, dir));
    }

    private static void findAllDirs(Set<FileSystemDirectory> directories, FileSystemDirectory cdir) {
        directories.add(cdir);
        cdir.lsdir().forEach(dir -> findAllDirs(directories, dir));
    }

}
