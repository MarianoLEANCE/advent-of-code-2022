package com.adventofcode.year_2022;

import io.vavr.Function1;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * @see <a href="https://adventofcode.com/2022/day/11">Day 11</a>
 */
public class Day11 {

    public Long part1() {
        return monkeySlingingShenanigans(input(), 20, true);
    }

    public Long part2() {
        return monkeySlingingShenanigans(input(), 10000, false);
    }

    public Long hint() {
        return monkeySlingingShenanigans(hintInput(), 10000, false);
    }

    private Long monkeySlingingShenanigans(List<Monkey> monkeys, int rounds, boolean relief) {
        Consumer<Monkey> doBusiness = monkey -> monkey.doBusiness(relief);
        for (int i = 0; i < rounds; i++) {
            monkeys.forEach(doBusiness);
        }
        return monkeys.stream()
                .map(Monkey::getInspections)
                .sorted(Collections.reverseOrder())
                .limit(2)
                .reduce((a, b) -> a * b)
                .orElse(0L);
    }

    @Getter
    @Setter
    @RequiredArgsConstructor
    private static class Monkey {

        private long inspections = 0;
        private final Queue<Long> items;
        private final Function1<Long, Long> operation;
        private final Predicate<Long> test;
        private Monkey onSuccessRecipient;
        private Monkey onFailureRecipient;

        public void doBusiness(boolean relief) {
            while (!items.isEmpty()) {
                long worry = items.remove();

                // monkey inspects the item
                inspections++;

                // worry increases
                worry = operation.apply(worry);

                // worry decreases
                if (relief) {
                    worry /= 3;
                }

                if (test.test(worry)) {
                    onSuccessRecipient.items.add(worry);
                } else {
                    onFailureRecipient.items.add(worry);
                }
            }
        }

    }

    private List<Monkey> input() {
        Monkey monkey0 = new Monkey(
                of(56, 52, 58, 96, 70, 75, 72),
                x -> x * 17,
                x -> x % 11 == 0);
        Monkey monkey1 = new Monkey(
                of(75, 58, 86, 80, 55, 81),
                x -> x + 7,
                x -> x % 3 == 0);
        Monkey monkey2 = new Monkey(
                of(73, 68, 73, 90),
                x -> x * x,
                x -> x % 5 == 0);
        Monkey monkey3 = new Monkey(
                of(72, 89, 55, 51, 59),
                x -> x + 1,
                x -> x % 7 == 0);
        Monkey monkey4 = new Monkey(
                of(76, 76, 91),
                x -> x * 3,
                x -> x % 19 == 0);
        Monkey monkey5 = new Monkey(
                of(88),
                x -> x + 4,
                x -> x % 2 == 0);
        Monkey monkey6 = new Monkey(
                of(64, 63, 56, 50, 77, 55, 55, 86),
                x -> x + 8,
                x -> x % 13 == 0);
        Monkey monkey7 = new Monkey(
                of(79, 58),
                x -> x + 6,
                x -> x % 17 == 0);

        monkey0.setOnSuccessRecipient(monkey2);
        monkey0.setOnFailureRecipient(monkey3);

        monkey1.setOnSuccessRecipient(monkey6);
        monkey1.setOnFailureRecipient(monkey5);

        monkey2.setOnSuccessRecipient(monkey1);
        monkey2.setOnFailureRecipient(monkey7);

        monkey3.setOnSuccessRecipient(monkey2);
        monkey3.setOnFailureRecipient(monkey7);

        monkey4.setOnSuccessRecipient(monkey0);
        monkey4.setOnFailureRecipient(monkey3);

        monkey5.setOnSuccessRecipient(monkey6);
        monkey5.setOnFailureRecipient(monkey4);

        monkey6.setOnSuccessRecipient(monkey4);
        monkey6.setOnFailureRecipient(monkey0);

        monkey7.setOnSuccessRecipient(monkey1);
        monkey7.setOnFailureRecipient(monkey5);

        return List.of(monkey0, monkey1, monkey2, monkey3,
                monkey4, monkey5, monkey6, monkey7);
    }

    private List<Monkey> hintInput() {
        Monkey monkey0 = new Monkey(
                of(79, 98),
                x -> x * 19,
                x -> x % 23 == 0);
        Monkey monkey1 = new Monkey(
                of(54, 65, 75, 74),
                x -> x + 6,
                x -> x % 19 == 0);
        Monkey monkey2 = new Monkey(
                of(79, 60, 97),
                x -> x * x,
                x -> x % 13 == 0);
        Monkey monkey3 = new Monkey(
                of(74),
                x -> x + 3,
                x -> x % 17 == 0);

        monkey0.setOnSuccessRecipient(monkey2);
        monkey0.setOnFailureRecipient(monkey3);

        monkey1.setOnSuccessRecipient(monkey2);
        monkey1.setOnFailureRecipient(monkey0);

        monkey2.setOnSuccessRecipient(monkey1);
        monkey2.setOnFailureRecipient(monkey3);

        monkey3.setOnSuccessRecipient(monkey0);
        monkey3.setOnFailureRecipient(monkey1);

        return List.of(monkey0, monkey1, monkey2, monkey3);
    }

    private Queue<Long> of(long... items) {
        return Arrays.stream(items).boxed().
                collect(Collectors.toCollection(ArrayDeque::new));
    }

}
