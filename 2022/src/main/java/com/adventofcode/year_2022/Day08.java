package com.adventofcode.year_2022;

import io.vavr.Function1;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

/**
 * todo refactor to pure code
 */
public class Day08 {

    public static Integer part1(String input) {
        return Function1.of(Day08::parse)
                .andThen(trees -> trees.stream().flatMap(List::stream).filter(Tree::isVisible).count())
                .andThen(Long::intValue)
                .apply(input);
    }

    public static Integer part2(String input) {
        return Function1.of(Day08::parse)
                .andThen(trees -> trees.stream()
                        .flatMap(List::stream)
                        .mapToInt(Tree::scenicScore)
                        .max()
                        .orElse(0))
                .apply(input);
    }

    private static List<List<Tree>> parse(String input) {
        String[] lines = input.split("\n");
        List<List<Tree>> trees = new ArrayList<>();
        for (int i = 0; i < lines.length; i++) {
            trees.add(new ArrayList<>());
            for (int j = 0; j < lines[i].length(); j++) {
                trees.get(i).add(new Tree(Integer.parseInt(lines[i].charAt(j) + "")));
                if (0 < i) {
                    trees.get(i).get(j).setNorth(trees.get(i - 1).get(j));
                }
                if (0 < j) {
                    trees.get(i).get(j).setWest(trees.get(i).get(j - 1));
                }
            }
        }
        return trees;
    }

    @Getter
    @Setter
    @RequiredArgsConstructor
    @ToString(of = {"height"})
    static class Tree {

        private final int height;

        private Tree north;
        private Tree east;
        private Tree south;
        private Tree west;

        public void setNorth(Tree north) {
            this.north = north;
            this.north.setSouth(this);
        }

        public void setWest(Tree west) {
            this.west = west;
            this.west.setEast(this);
        }

        public boolean isVisible() {
            return isVisibleFrom(height, this, Tree::getNorth) ||
                    isVisibleFrom(height, this, Tree::getEast) ||
                    isVisibleFrom(height, this, Tree::getSouth) ||
                    isVisibleFrom(height, this, Tree::getWest);
        }

        private boolean isVisibleFrom(int height, Tree tree, Function<Tree, Tree> f) {
            Tree next = f.apply(tree);
            if (next == null) {
                return true;
            }
            if (height <= next.getHeight()) {
                return false;
            }
            return isVisibleFrom(height, next, f);
        }

        public int scenicScore() {
            return scenicScore(this.height, this, Tree::getNorth) *
                    scenicScore(this.height, this, Tree::getEast) *
                    scenicScore(this.height, this, Tree::getWest) *
                    scenicScore(this.height, this, Tree::getSouth);
        }

        private int scenicScore(int height, Tree tree, Function<Tree, Tree> f) {
            Tree next = f.apply(tree);
            if (next == null) {
                return 0;
            }
            if (next.getHeight() >= height) {
                return 1;
            }
            return 1 + scenicScore(height, next, f);
        }

    }

}
