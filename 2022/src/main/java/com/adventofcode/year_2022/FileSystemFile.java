package com.adventofcode.year_2022;

public class FileSystemFile implements FileSystemElement {

    private final String name;
    private final int size;

    public FileSystemFile(String name, int size) {
        this.name = name;
        this.size = size;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public int size() {
        return size;
    }

}
