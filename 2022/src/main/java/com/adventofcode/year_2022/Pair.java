package com.adventofcode.year_2022;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@AllArgsConstructor
@EqualsAndHashCode
public class Pair<A, B> {

    private final A a;
    private final B b;

}
