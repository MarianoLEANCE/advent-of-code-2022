package com.adventofcode.year_2022;

import io.vavr.Function2;
import io.vavr.Function3;

import java.util.stream.IntStream;

/**
 * @see <a href="https://adventofcode.com/2022/day/6">Day 6</a>
 */
public class Day06 {

    public static Integer part1(String input) {
        return startOfMessage(4, input);
    }

    public static Integer part2(String input) {
        return startOfMessage(14, input);
    }

    private static int startOfMessage(int markerLength, String input) {
        var isMarker = Function3.of(Day06::isMarker).curried().apply(markerLength).apply(input);
        return IntStream.range(0, input.length() - markerLength).boxed()
                .filter(isMarker::apply)
                .map(Function2.of(Day06::add).curried().apply(markerLength))
                .findFirst()
                .orElse(0);
    }

    private static int add(int x, int y) {
        return x + y;
    }

    private static boolean isMarker(int markerLength, String input, int index) {
        long distinctChars = input.substring(index, index + markerLength).chars().boxed().distinct().count();
        return distinctChars == markerLength;
    }

}
