package com.adventofcode.year_2022;

import io.vavr.Function1;
import io.vavr.collection.Array;
import io.vavr.collection.Traversable;

/**
 * @see <a href="https://adventofcode.com/2022/day/2">Day 2</a>
 */
public class Day02 {

    public static Integer part1(String input) {
        return Function1.of(Day02::rounds)
                .andThen(x -> x.map(Day02::play1))
                .andThen(Traversable::sum)
                .apply(input)
                .intValue();
    }

    public static Integer part2(String input) {
        return Function1.of(Day02::rounds)
                .andThen(x -> x.map(Day02::play2))
                .andThen(Traversable::sum)
                .apply(input)
                .intValue();
    }

    private static Array<String> rounds(String input) {
        return Array.of(input.split("\n"));
    }

    private static Integer play1(String input) {
        Hand opponent = parseOpponent(input.substring(0, 1));
        Hand me = parseMe(input.substring(2, 3));
        return play(opponent, me);
    }

    private static Integer play2(String input) {
        Hand opponent = parseOpponent(input.substring(0, 1));
        Hand me = parseMe(opponent, input.substring(2, 3));
        return play(opponent, me);
    }

    private static Hand parseOpponent(String input) {
        return switch (input) {
            case "A" -> Hand.ROCK;
            case "B" -> Hand.PAPER;
            case "C" -> Hand.SCISSORS;
            default -> throw new IllegalArgumentException();
        };
    }

    private static Hand parseMe(String input) {
        return switch (input) {
            case "X" -> Hand.ROCK;
            case "Y" -> Hand.PAPER;
            case "Z" -> Hand.SCISSORS;
            default -> throw new IllegalArgumentException();
        };
    }

    private static Hand parseMe(Hand opponent, String input) {
        return switch (input) {
            case "X" -> switch (opponent) {
                case ROCK -> Hand.SCISSORS;
                case PAPER -> Hand.ROCK;
                case SCISSORS -> Hand.PAPER;
            };
            case "Y" -> switch (opponent) {
                case ROCK -> Hand.ROCK;
                case PAPER -> Hand.PAPER;
                case SCISSORS -> Hand.SCISSORS;
            };
            case "Z" -> switch (opponent) {
                case ROCK -> Hand.PAPER;
                case PAPER -> Hand.SCISSORS;
                case SCISSORS -> Hand.ROCK;
            };
            default -> throw new IllegalArgumentException();
        };
    }

    private static final int LOSE = 0;
    private static final int DRAW = 3;
    private static final int WIN = 6;

    private static Integer play(Hand opponent, Hand me) {
        return me.value + switch (opponent) {
            case ROCK -> switch (me) {
                case ROCK -> DRAW;
                case PAPER -> WIN;
                case SCISSORS -> LOSE;
            };
            case PAPER -> switch (me) {
                case ROCK -> LOSE;
                case PAPER -> DRAW;
                case SCISSORS -> WIN;
            };
            case SCISSORS -> switch (me) {
                case ROCK -> WIN;
                case PAPER -> LOSE;
                case SCISSORS -> DRAW;
            };
        };
    }

    private enum Hand {
        ROCK(1),
        PAPER(2),
        SCISSORS(3);

        final int value;

        Hand(int value) {
            this.value = value;
        }

    }

}
