package com.adventofcode.year_2022.utils;

import io.vavr.Function1;
import io.vavr.Function2;
import io.vavr.collection.Array;

public class ArrayCurryUtils {

    public static <T> Function1<Integer, Function1<Array<T>, T>> get() {
        return Function2.<Integer, Array<T>, T>of((index, array) -> array.get(index)).curried();
    }

}
