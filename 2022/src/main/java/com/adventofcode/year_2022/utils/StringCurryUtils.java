package com.adventofcode.year_2022.utils;

import io.vavr.Function1;
import io.vavr.Function2;

public class StringCurryUtils {

    public static Function1<String, Function1<String, String[]>> split() {
        return Function2.<String, String, String[]>of((pattern, string) -> string.split(pattern)).curried();
    }

}
