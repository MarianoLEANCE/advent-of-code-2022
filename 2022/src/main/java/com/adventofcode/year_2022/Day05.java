package com.adventofcode.year_2022;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @see <a href="https://adventofcode.com/2022/day/4">Day 4</a>
 */
public class Day05 {

    public static String part1(String input) {
        Map<Integer, Stack<String>> stacks = new TreeMap<>();
        for (int i = 1; i <= 9; i++) {
            stacks.put(i, parseStacks(1 + (i - 1) * 4, input));
        }

        executeSingleMoves(stacks, input);
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 1; i <= 9; i++) {
            stringBuilder.append(stacks.get(i).pop());
        }
        return stringBuilder.toString();
    }

    public static String part2(String input) {
        Map<Integer, Stack<String>> stacks = new TreeMap<>();
        for (int i = 1; i <= 9; i++) {
            stacks.put(i, parseStacks(1 + (i - 1) * 4, input));
        }

        executeMultipleMoves(stacks, input);
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 1; i <= 9; i++) {
            stringBuilder.append(stacks.get(i).pop());
        }
        return stringBuilder.toString();
    }

    private static Stack<String> parseStacks(int index, String input) {
        String[] lines = input.split("\n");
        Stack<String> stack = new Stack<>();
        for (int i = 7; i >= 0; i--) {
            if (lines[i].charAt(index) != ' ') {
                stack.add("" + lines[i].charAt(index));
            }
        }
        return stack;
    }

    private static void executeSingleMoves(Map<Integer, Stack<String>> stacks, String moves) {
        Pattern p = Pattern.compile("move (\\d+) from (\\d+) to (\\d+)");
        Arrays.stream(moves.split("\n")).skip(10)
                .forEach(move -> {
                    Matcher matcher = p.matcher(move);
                    matcher.matches();
                    int quantity = Integer.parseInt(matcher.group(1));
                    int from = Integer.parseInt(matcher.group(2));
                    int to = Integer.parseInt(matcher.group(3));
                    for (int i = 0; i < quantity; i++) {
                        String item = stacks.get(from).pop();
                        stacks.get(to).add(item);
                    }
                });
    }

    private static void executeMultipleMoves(Map<Integer, Stack<String>> stacks, String moves) {
        Pattern p = Pattern.compile("move (\\d+) from (\\d+) to (\\d+)");
        Arrays.stream(moves.split("\n")).skip(10)
                .forEach(move -> {
                    Matcher matcher = p.matcher(move);
                    matcher.matches();
                    int quantity = Integer.parseInt(matcher.group(1));
                    int from = Integer.parseInt(matcher.group(2));
                    int to = Integer.parseInt(matcher.group(3));
                    List<String> crates = new ArrayList<>();

                    for (int i = 0; i < quantity; i++) {
                        String item = stacks.get(from).pop();
                        crates.add(0, item);
                    }
                    while (!crates.isEmpty()) {
                        stacks.get(to).add(crates.remove(0));
                    }
                });
    }

}
