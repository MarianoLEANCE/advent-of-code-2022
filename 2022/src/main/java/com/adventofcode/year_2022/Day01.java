package com.adventofcode.year_2022;

import io.vavr.Function1;
import io.vavr.collection.Array;
import io.vavr.collection.Traversable;

import java.util.Arrays;

/**
 * @see <a href="https://adventofcode.com/2022/day/1">Day 1</a>
 */
public class Day01 {

    public static Integer part1(String input) {
        return Function1.of(Day01::backpacks)
                .andThen(backpacks -> backpacks.map(Day01::calories))
                .andThen(Traversable::max)
                .andThen(totalCalories -> totalCalories.getOrElse(0))
                .apply(input);
    }

    public static Integer part2(String input) {
        return Function1.of(Day01::backpacks)
                .andThen(backpacks -> backpacks.map(Day01::calories))
                .andThen(Array::sorted)
                .andThen(Array::reverse)
                .andThen(calories -> calories.take(3))
                .andThen(Traversable::sum)
                .andThen(Number::intValue)
                .apply(input);
    }

    private static Array<String> backpacks(String input) {
        return Array.of(input.split("\n\n"));
    }

    private static Integer calories(String backpack) {
        return Arrays.stream(backpack.split("\n")).mapToInt(Integer::parseInt).sum();
    }

}
