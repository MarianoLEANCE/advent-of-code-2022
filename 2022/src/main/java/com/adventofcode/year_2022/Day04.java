package com.adventofcode.year_2022;

import com.adventofcode.year_2022.utils.ArrayCurryUtils;
import com.adventofcode.year_2022.utils.StringCurryUtils;
import io.vavr.Function1;
import io.vavr.Tuple2;
import io.vavr.collection.Array;
import io.vavr.collection.List;
import lombok.Getter;

/**
 * @see <a href="https://adventofcode.com/2022/day/4">Day 4</a>
 */
public class Day04 {

    public static Integer part1(String input) {
        return Function1.of(Day04::assignements)
                .andThen(assignments -> assignments.map(Day04::assignement))
                .andThen(pairs -> pairs.count(Day04::oneIsContained))
                .apply(input);
    }

    private static boolean oneIsContained(Tuple2<Range<Integer>, Range<Integer>> assignment) {
        return assignment._1().contains(assignment._2()) || assignment._2().contains(assignment._1());
    }

    public static Integer part2(String input) {
        return Function1.of(Day04::assignements)
                .andThen(assignments -> assignments.map(Day04::assignement))
                .andThen(pairs -> pairs.count(pair -> pair._1().overlap(pair._2())))
                .apply(input);
    }

    private static List<String> assignements(String input) {
        return List.of(input.split("\n"));
    }

    private static Tuple2<Range<Integer>, Range<Integer>> assignement(String assignment) {
        return new Tuple2<>(
                new Range<>(parseBound(0, 0, assignment), parseBound(0, 1, assignment)),
                new Range<>(parseBound(1, 0, assignment), parseBound(1, 1, assignment))
        );
    }

    private static int parseBound(int elf, int bound, String assignment) {
        return StringCurryUtils.split().apply(",").andThen(Array::of)
                .andThen(ArrayCurryUtils.<String>get().apply(elf))
                .andThen(StringCurryUtils.split().apply("-")).andThen(Array::of)
                .andThen(ArrayCurryUtils.<String>get().apply(bound))
                .andThen(Integer::parseInt)
                .apply(assignment);
    }

    @Getter
    public static class Range<T extends Comparable<T>> {

        private final T lower;
        private final T upper;

        public Range(T lower, T upper) {
            this.lower = lower;
            this.upper = upper;
        }

        public boolean contains(Range<T> range) {
            return lower.compareTo(range.getLower()) <= 0 && upper.compareTo(range.getUpper()) >= 0;
        }

        public boolean contains(T x) {
            return lower.compareTo(x) <= 0 && upper.compareTo(x) >= 0;
        }

        public boolean overlap(Range<T> range) {
            return contains(range.getLower()) || contains(range.getUpper())
                    || range.contains(getLower()) || range.contains(getUpper());
        }

        @Override
        public String toString() {
            return lower + "-" + upper;
        }

    }

}
