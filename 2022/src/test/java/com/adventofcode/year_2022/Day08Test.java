package com.adventofcode.year_2022;

import com.adventofcode.common.InputUtils;
import io.vavr.control.Try;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day08Test {

    @Test
    void part1() {
        int expected = input().map(Day08::part1).get();
        assertEquals(1711, expected);
    }

    @Test
    void part2() {
        int expected = input().map(Day08::part2).get();
        assertEquals(301392, expected);
    }

    private Try<String> input() {
        return InputUtils.readFromClassPath("day08.txt");
    }

}
