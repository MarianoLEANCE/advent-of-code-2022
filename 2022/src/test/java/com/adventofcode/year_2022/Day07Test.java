package com.adventofcode.year_2022;

import com.adventofcode.common.InputUtils;
import io.vavr.control.Try;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day07Test {

    @Test
    void part1() {
        int expected = input().map(Day07::part1).get();
        assertEquals(1583951, expected);
    }

    @Test
    void part2() {
        int expected = input().map(Day07::part2).get();
        assertEquals(214171, expected);
    }

    private Try<String> input() {
        return InputUtils.readFromClassPath("day07.txt");
    }

}
