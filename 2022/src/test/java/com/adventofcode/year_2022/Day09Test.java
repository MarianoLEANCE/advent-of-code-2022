package com.adventofcode.year_2022;

import com.adventofcode.common.InputUtils;
import io.vavr.control.Try;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day09Test {

    @Test
    void part1() throws IOException {
        int expected = input().map(Day09::part1).get();
        assertEquals(6337, expected);
    }

    @Test
    void part2() throws IOException {
        int expected = input().map(Day09::part2).get();
        assertEquals(2455, expected);
    }

    private Try<String> input() {
        return InputUtils.readFromClassPath("day09.txt");
    }

}
