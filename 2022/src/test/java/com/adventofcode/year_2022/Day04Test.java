package com.adventofcode.year_2022;

import com.adventofcode.common.InputUtils;
import io.vavr.control.Try;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day04Test {

    @Test
    void part1() {
        int expected = input().map(Day04::part1).get();
        assertEquals(573, expected);
    }

    @Test
    void part2() {
        int expected = input().map(Day04::part2).get();
        assertEquals(867, expected);
    }

    private Try<String> input() {
        return InputUtils.readFromClassPath("day04.txt");
    }

}
