package com.adventofcode.year_2022;

import com.adventofcode.common.InputUtils;
import io.vavr.control.Try;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class Day10Test {

    @Test
    void part1() throws IOException {
        int actual = input().map(Day10::part1).get();
        assertEquals(14520, actual);
    }

    @Test
    void part2() {
        String expected = """
                ###..####.###...##..####.####...##.###..
                #..#....#.#..#.#..#....#.#.......#.#..#.
                #..#...#..###..#......#..###.....#.###..
                ###...#...#..#.#.##..#...#.......#.#..#.
                #....#....#..#.#..#.#....#....#..#.#..#.
                #....####.###...###.####.####..##..###..
                """;
        String actual = input().map(Day10::part2).get();

        assertTrue(actual.contains(expected));
    }

    private Try<String> input() {
        return InputUtils.readFromClassPath("day10.txt");
    }

}
