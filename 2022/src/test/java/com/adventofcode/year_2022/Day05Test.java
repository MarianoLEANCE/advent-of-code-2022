package com.adventofcode.year_2022;

import com.adventofcode.common.InputUtils;
import io.vavr.control.Try;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day05Test {

    @Test
    void part1() {
        String expected = input().map(Day05::part1).get();
        assertEquals("CFFHVVHNC", expected);
    }

    @Test
    void par2() {
        String expected = input().map(Day05::part2).get();
        assertEquals("FSZWBPTBG", expected);
    }

    private Try<String> input() {
        return InputUtils.readFromClassPath("day05.txt");
    }

}
