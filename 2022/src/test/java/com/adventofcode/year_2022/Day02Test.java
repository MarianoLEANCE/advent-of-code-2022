package com.adventofcode.year_2022;

import com.adventofcode.common.InputUtils;
import io.vavr.control.Try;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day02Test {

    @Test
    void part1() {
        int expected = input().map(Day02::part1).get();
        assertEquals(13924, expected);
    }

    @Test
    void part2() {
        int expected = input().map(Day02::part2).get();
        assertEquals(13448, expected);
    }

    private Try<String> input() {
        return InputUtils.readFromClassPath("day02.txt");
    }

}
