package com.adventofcode.year_2015;

import com.adventofcode.common.ArrayUtils;
import io.vavr.Function1;
import io.vavr.Function3;
import io.vavr.collection.Array;

public class Day01 {

    public int part1(String input) {
        return Function1.of(String::toCharArray)
                .andThen(Array::ofAll)
                .andThen(ArrayUtils.map(this::changeFloor))
                .andThen(Array::sum)
                .andThen(Number::intValue)
                .apply(input);
    }

    private int changeFloor(char instruction) {
        return switch (instruction) {
            case '(' -> 1;
            case ')' -> -1;
            default -> 0;
        };
    }

    public int part2(String input) {
        return Function1.of(String::toCharArray)
                .andThen(Array::ofAll)
                .andThen(Function3.of(this::entersBasement).curried().apply(0).apply(0))
                .apply(input);
    }

    public int entersBasement(int position, int floor, Array<Character> instructions) {
        if (floor == -1) {
            return position;
        }
        Character instruction = instructions.get();
        int nextFloor = floor + changeFloor(instruction);
        return entersBasement(position + 1, nextFloor, instructions.subSequence(1));
    }

}
