package com.adventofcode.year_2015;

import com.adventofcode.common.ArrayUtils;
import com.adventofcode.common.CombinationUtils;
import com.adventofcode.common.StringUtils;
import io.vavr.Function1;
import io.vavr.collection.Array;
import io.vavr.collection.Map;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day15 {

    public long part1(String input) {
        return Function1.of(StringUtils.split("\n"))
                .andThen(ArrayUtils.map(this::parse))
                .andThen(this::maximiseScore)
                .apply(input);
    }

    private Ingredient parse(String line) {
        Pattern pattern = Pattern.compile("([a-zA-Z]+): capacity (-?\\d+)," +
                                          " durability (-?\\d+)," +
                                          " flavor (-?\\d+)," +
                                          " texture (-?\\d+)," +
                                          " calories (-?\\d+)");
        Matcher matcher = pattern.matcher(line);
        matcher.matches();
        return new Ingredient(matcher.group(1),
                Integer.parseInt(matcher.group(2)),
                Integer.parseInt(matcher.group(3)),
                Integer.parseInt(matcher.group(4)),
                Integer.parseInt(matcher.group(5)),
                Integer.parseInt(matcher.group(6)));
    }

    private long maximiseScore(Array<Ingredient> ingredients) {
        return CombinationUtils.combinationsCount(ingredients, 100).map(this::score).max().getOrElse(0L);
    }

    private long score(Map<Ingredient, Integer> ingredients) {
        return score(ingredients, Ingredient::capacity) *
               score(ingredients, Ingredient::durability) *
               score(ingredients, Ingredient::flavor) *
               score(ingredients, Ingredient::texture);
    }

    private long score(Map<Ingredient, Integer> ingredients, Function1<Ingredient, Integer> property) {
        long score = ingredients
                .map(ingredient -> ingredient._2() * property.apply(ingredient._1()))
                .sum()
                .longValue();
        return Math.max(0, score);
    }

    private record Ingredient(String name,
                              int capacity,
                              int durability,
                              int flavor,
                              int texture,
                              int calories) {}

    public long part2(String input) {
        return Function1.of(StringUtils.split("\n"))
                .andThen(ArrayUtils.map(this::parse))
                .andThen(this::maximiseDietScore)
                .apply(input);
    }

    private long maximiseDietScore(Array<Ingredient> ingredients) {
        return CombinationUtils.combinationsCount(ingredients, 100)
                .filter(this::has500kCal)
                .map(this::score).max().getOrElse(0L);
    }

    private boolean has500kCal(Map<Ingredient, Integer> ingredients) {
        long score = ingredients
                .map(ingredient -> ingredient._2() * ingredient._1().calories())
                .sum()
                .longValue();
        return score == 500;
    }

}
