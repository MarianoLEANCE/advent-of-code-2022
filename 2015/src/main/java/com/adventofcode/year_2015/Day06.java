package com.adventofcode.year_2015;

import io.vavr.Function1;
import io.vavr.Function3;
import io.vavr.Function4;
import io.vavr.Tuple2;
import io.vavr.collection.Array;
import io.vavr.collection.Traversable;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day06 {

    public int part1(String input) {
        return Function1.of(this::parse)
                .andThen(instructions -> instructions.reduceLeft(Function1::andThen))
                .andThen(f -> f.apply(initGrid()))
                .andThen(grid -> grid.count(x -> x))
                .apply(input);
    }

    private Array<Boolean> initGrid() {
        return Array.range(0, 1_000_000).map(x -> false);
    }

    private Array<Boolean> turnOn(Tuple2<Integer, Integer> from, Tuple2<Integer, Integer> to, Array<Boolean> grid) {
        return grid.zipWithIndex().map(x -> in(from, to, x._2()) || x._1());
    }

    private Array<Boolean> turnOff(Tuple2<Integer, Integer> from, Tuple2<Integer, Integer> to, Array<Boolean> grid) {
        return grid.zipWithIndex().map(x -> !in(from, to, x._2()) && x._1());
    }

    private Array<Boolean> toggle(Tuple2<Integer, Integer> from, Tuple2<Integer, Integer> to, Array<Boolean> grid) {
        return grid.zipWithIndex().map(x -> in(from, to, x._2()) != x._1());
    }

    private boolean in(Tuple2<Integer, Integer> from, Tuple2<Integer, Integer> to, int index) {
        int col = index % 1000;
        int row = index / 1000;
        return from._1() <= col && col <= to._1() &&
               from._2() <= row && row <= to._2();
    }

    private Array<Function1<Array<Boolean>, Array<Boolean>>> parse(String input) {
        return Array.of(input.split("\n")).map(this::parseInstruction);
    }

    private Function1<Array<Boolean>, Array<Boolean>> parseInstruction(String line) {
        var function = instruction(line);
        var coordinates = coordinates(line);
        return function.apply(coordinates._1()).apply(coordinates._2());
    }

    private Function3<Tuple2<Integer, Integer>, Tuple2<Integer, Integer>, Array<Boolean>, Array<Boolean>> instruction(
            String line) {
        if (line.startsWith("turn on")) {
            return Function3.of(this::turnOn).memoized();
        }
        if (line.startsWith("turn off")) {
            return Function3.of(this::turnOff).memoized();
        }
        return Function3.of(this::toggle).memoized();
    }

    private Tuple2<Tuple2<Integer, Integer>, Tuple2<Integer, Integer>> coordinates(String line) {
        Pattern p = Pattern.compile("(turn on|turn off|toggle) (\\d+),(\\d+) through (\\d+),(\\d+)");
        Matcher m = p.matcher(line);
        m.find();
        return new Tuple2<>(
                new Tuple2<>(Integer.parseInt(m.group(2)), Integer.parseInt(m.group(3))),
                new Tuple2<>(Integer.parseInt(m.group(4)), Integer.parseInt(m.group(5))));
    }

    public long part2(String input) {
        return Function1.of(this::parseBrightness)
                .andThen(instructions -> instructions.reduceLeft(Function1::andThen))
                .andThen(f -> f.apply(initBrightnessGrid()))
                .andThen(Traversable::sum)
                .andThen(Number::longValue)
                .apply(input);
    }

    private Array<Function1<Array<Long>, Array<Long>>> parseBrightness(String input) {
        return Array.of(input.split("\n")).map(this::parseBrightnessInstruction);
    }

    private Function1<Array<Long>, Array<Long>> parseBrightnessInstruction(String line) {
        var function = brightnessInstruction(line);
        var coordinates = coordinates(line);
        return function.apply(coordinates._1()).apply(coordinates._2());
    }

    private Function3<Tuple2<Integer, Integer>, Tuple2<Integer, Integer>, Array<Long>, Array<Long>>
    brightnessInstruction(String line) {
        if (line.startsWith("turn on")) {
            return Function4.of(this::changeBrightness).memoized().apply(1);
        }
        if (line.startsWith("turn off")) {
            return Function4.of(this::changeBrightness).memoized().apply(-1);
        }
        return Function4.of(this::changeBrightness).memoized().apply(2);
    }

    private Array<Long> changeBrightness(int change, Tuple2<Integer, Integer> from, Tuple2<Integer, Integer> to,
                                         Array<Long> grid) {
        return grid.zipWithIndex().map(x -> in(from, to, x._2())
                ? Math.max(x._1() + change, 0)
                : x._1());
    }

    private Array<Long> initBrightnessGrid() {
        return Array.range(0, 1_000_000).map(x -> 0L);
    }

}
