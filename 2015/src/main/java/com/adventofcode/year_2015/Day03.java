package com.adventofcode.year_2015;

import com.adventofcode.common.StringUtils;
import io.vavr.Function1;
import io.vavr.Tuple2;
import io.vavr.collection.Array;
import io.vavr.collection.Traversable;

public class Day03 {

    public int part1(String input) {
        return Function1.of(StringUtils.replace("\n", ""))
                .andThen(this::directions)
                .andThen(this::coordinates)
                .andThen(Array::distinct)
                .andThen(Traversable::size)
                .andThen(Number::intValue)
                .apply(input);
    }

    private Array<Function1<Tuple2<Integer, Integer>, Tuple2<Integer, Integer>>> directions(String input) {
        return Array.ofAll(input.toCharArray())
                .map(c -> switch (c) {
                    case '>' -> h -> new Tuple2<>(h._1() + 1, h._2());
                    case '<' -> h -> new Tuple2<>(h._1() - 1, h._2());
                    case '^' -> h -> new Tuple2<>(h._1(), h._2() + 1);
                    case 'v' -> h -> new Tuple2<>(h._1(), h._2() - 1);
                    default -> throw new IllegalStateException("Unexpected value: " + c);
                });
    }

    private Array<Tuple2<Integer, Integer>> coordinates(Array<Function1<Tuple2<Integer, Integer>,
            Tuple2<Integer, Integer>>> directions) {
        return directions.foldLeft(Array.of(new Tuple2<>(0, 0)),
                (xs, x) -> xs.append(x.apply(xs.last())));
    }

    public int part2(String input) {
        return Function1.of(StringUtils.replace("\n", ""))
                .andThen(this::directions)
                .andThen(this::splitJobs)
                .andThen(tuple -> tuple.map(this::coordinates, this::coordinates))
                .andThen(tuple-> tuple._1().appendAll(tuple._2()))
                .andThen(Array::distinct)
                .andThen(Traversable::size)
                .andThen(Number::intValue)
                .apply(input);
    }

    private Tuple2<Array<Function1<Tuple2<Integer, Integer>, Tuple2<Integer, Integer>>>,
            Array<Function1<Tuple2<Integer, Integer>, Tuple2<Integer, Integer>>>> splitJobs(
            Array<Function1<Tuple2<Integer, Integer>, Tuple2<Integer, Integer>>> directions) {
        var zipped = directions.zipWithIndex();
        var partitioned = zipped.partition(tuple -> tuple._2() % 2 == 0);

        return new Tuple2<>(partitioned._1().map(Tuple2::_1), partitioned._2().map(Tuple2::_1));
    }

//    private int requiredPaper(Tuple3<Integer, Integer, Integer> box) {
//        return surface(box) + slack(box);
//    }
//
//    private int surface(Tuple3<Integer, Integer, Integer> box) {
//        int l = box._1();
//        int w = box._2();
//        int h = box._3();
//        return 2 * l * w + 2 * w * h + 2 * h * l;
//    }
//
//    private int slack(Tuple3<Integer, Integer, Integer> box) {
//        int l = box._1();
//        int w = box._2();
//        int h = box._3();
//        Array<Integer> sorted = Array.of(l, w, h).sorted();
//        return sorted.get(0) * sorted.get(1);
//    }
//
//    public int part2(String input) {
//        return Function1.of(StringUtils.split("\n"))
//                .andThen(ArrayUtils.map(this::boxes))
//                .andThen(ArrayUtils.map(this::requiredRibbon))
//                .andThen(Array::sum)
//                .andThen(Number::intValue)
//                .apply(input);
//    }
//
//    private int requiredRibbon(Tuple3<Integer, Integer, Integer> box) {
//        return wrapAround(box) + bow(box);
//    }
//
//    private int wrapAround(Tuple3<Integer, Integer, Integer> box) {
//        int l = box._1();
//        int w = box._2();
//        int h = box._3();
//        Array<Integer> sorted = Array.of(l, w, h).sorted();
//        return 2 * sorted.get(0) + 2 * sorted.get(1);
//    }
//
//    private int bow(Tuple3<Integer, Integer, Integer> box) {
//        int l = box._1();
//        int w = box._2();
//        int h = box._3();
//        return l * w * h;
//    }

}
