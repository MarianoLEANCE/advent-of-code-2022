package com.adventofcode.year_2015;

import com.adventofcode.common.StringUtils;
import io.vavr.Function1;
import io.vavr.collection.Array;

import java.util.Objects;

public class Day05 {

    public int part1(String input) {
        return Function1.of(StringUtils.split("\n"))
                .andThen(Array::ofAll)
                .andThen(arr -> arr.count(this::nice))
                .apply(input);
    }

    private boolean nice(String s) {
        return Array.of(Function1.of(this::threeVowels),
                        Function1.of(this::twiceInARow),
                        Function1.of(this::notSpecific))
                .map(f -> f.apply(s))
                .reduce(Boolean::logicalAnd);
    }

    private boolean threeVowels(String s) {
        Array<Character> vowels = Array.of('a', 'e', 'i', 'o', 'u');
        Array<Character> chars = Array.ofAll(s.toCharArray());
        return chars.count(vowels::contains) >= 3;
    }

    private boolean twiceInARow(String s) {
        Array<Character> chars = Array.ofAll(s.toCharArray());
        var couples = chars.subSequence(1).zip(chars.subSequence(0, chars.size() - 1));
        return couples.count(couple -> Objects.equals(couple._1(), couple._2())) > 0;
    }

    private boolean notSpecific(String s) {
        return !Array.of("ab", "cd", "pq", "xy")
                .map(s::contains)
                .reduce(Boolean::logicalOr);
    }

    public int part2(String input) {
        return Function1.of(StringUtils.split("\n"))
                .andThen(Array::ofAll)
                .andThen(arr -> arr.count(this::nicer))
                .apply(input);
    }

    private boolean nicer(String s) {
        return Array.of(Function1.of(this::twoPairsTwice),
                        Function1.of(this::repeatWithOneLetterBetween))
                .map(f -> f.apply(s))
                .reduce(Boolean::logicalAnd);
    }

    private boolean twoPairsTwice(String s) {
        return Array.rangeClosed(0, s.length() - 2)
                .map(i -> {
                    String pair = s.substring(i, i + 2);
                    return s.indexOf(pair, i + 2) > 0;
                })
                .reduce(Boolean::logicalOr);
    }

    private boolean repeatWithOneLetterBetween(String s) {
        Array<Character> chars = Array.ofAll(s.toCharArray());
        var couples = chars.subSequence(0, chars.size() - 2).zip(chars.subSequence(2));
        return couples.count(couple -> Objects.equals(couple._1(), couple._2())) > 0;
    }

}
