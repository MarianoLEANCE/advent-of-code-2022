package com.adventofcode.year_2015;

import com.adventofcode.common.ArrayUtils;
import com.adventofcode.common.StringUtils;
import io.vavr.Function1;
import io.vavr.Tuple3;
import io.vavr.collection.Array;

public class Day02 {

    public int part1(String input) {
        return Function1.of(StringUtils.split("\n"))
                .andThen(ArrayUtils.map(this::boxes))
                .andThen(ArrayUtils.map(this::requiredPaper))
                .andThen(Array::sum)
                .andThen(Number::intValue)
                .apply(input);
    }

    private Tuple3<Integer, Integer, Integer> boxes(String dimensions) {
        String[] sides = dimensions.split("x");
        return new Tuple3<>(Integer.parseInt(sides[0]), Integer.parseInt(sides[1]), Integer.parseInt(sides[2]));
    }

    private int requiredPaper(Tuple3<Integer, Integer, Integer> box) {
        return surface(box) + slack(box);
    }

    private int surface(Tuple3<Integer, Integer, Integer> box) {
        int l = box._1();
        int w = box._2();
        int h = box._3();
        return 2 * l * w + 2 * w * h + 2 * h * l;
    }

    private int slack(Tuple3<Integer, Integer, Integer> box) {
        int l = box._1();
        int w = box._2();
        int h = box._3();
        Array<Integer> sorted = Array.of(l, w, h).sorted();
        return sorted.get(0) * sorted.get(1);
    }

    public int part2(String input) {
        return Function1.of(StringUtils.split("\n"))
                .andThen(ArrayUtils.map(this::boxes))
                .andThen(ArrayUtils.map(this::requiredRibbon))
                .andThen(Array::sum)
                .andThen(Number::intValue)
                .apply(input);
    }

    private int requiredRibbon(Tuple3<Integer, Integer, Integer> box) {
        return wrapAround(box) + bow(box);
    }

    private int wrapAround(Tuple3<Integer, Integer, Integer> box) {
        int l = box._1();
        int w = box._2();
        int h = box._3();
        Array<Integer> sorted = Array.of(l, w, h).sorted();
        return 2 * sorted.get(0) + 2 * sorted.get(1);
    }

    private int bow(Tuple3<Integer, Integer, Integer> box) {
        int l = box._1();
        int w = box._2();
        int h = box._3();
        return l * w * h;
    }

}
