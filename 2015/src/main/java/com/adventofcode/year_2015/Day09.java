package com.adventofcode.year_2015;

import com.adventofcode.common.ArrayUtils;
import com.adventofcode.common.StringUtils;
import io.vavr.Function1;
import io.vavr.Function3;
import io.vavr.Tuple2;
import io.vavr.Tuple3;
import io.vavr.collection.Array;

public class Day09 {

    public int part1(String input) {
        return Function1.of(StringUtils.split("\n"))
                .andThen(ArrayUtils.map(this::parse))
                .andThen(this::shortestDistance)
                .apply(input);
    }

    private Tuple3<String, String, Integer> parse(String line) {
        String[] chunks = line.replace(" to ", " ")
                .replace(" = ", " ")
                .split(" ");
        return new Tuple3<>(chunks[0], chunks[1], Integer.parseInt(chunks[2]));
    }

    private int shortestDistance(Array<Tuple3<String, String, Integer>> distances) {
        Array<Array<String>> permutations = distances.flatMap(line -> Array.of(line._1(), line._2()))
                .distinct()
                .permutations();
        return permutations.map(permutation -> distance(distances, permutation)).min().getOrElse(0);
    }

    private int distance(Array<Tuple3<String, String, Integer>> distances, Array<String> permutation) {
        var distance = Function3.of(this::getDistance).apply(distances).memoized();
        Array<Tuple2<String, String>> segments = permutation.subSequence(0, permutation.size() - 1)
                .zip(permutation.subSequence(1));
        return segments.map(segment -> distance.apply(segment._1(), segment._2())).sum().intValue();
    }

    private int getDistance(Array<Tuple3<String, String, Integer>> distances, String from, String to) {
        return distances.filter(distance -> (distance._1().equals(from) && distance._2().equals(to))
                                            || (distance._1().equals(to) && distance._2().equals(from)))
                .map(Tuple3::_3)
                .min().getOrElse(0);
    }

    public int part2(String input) {
        return Function1.of(StringUtils.split("\n"))
                .andThen(ArrayUtils.map(this::parse))
                .andThen(this::longestDistance)
                .apply(input);
    }

    private int longestDistance(Array<Tuple3<String, String, Integer>> distances) {
        Array<Array<String>> permutations = distances.flatMap(line -> Array.of(line._1(), line._2()))
                .distinct()
                .permutations();
        return permutations.map(permutation -> distance(distances, permutation)).max().getOrElse(0);
    }

}
