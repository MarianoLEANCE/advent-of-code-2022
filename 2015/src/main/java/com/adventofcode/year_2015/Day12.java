package com.adventofcode.year_2015;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.vavr.Function1;
import io.vavr.collection.Array;
import io.vavr.control.Try;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day12 {

    public int part1(String input) {
        return findNumbers(input)
                .sum()
                .intValue();
    }

    private Array<Integer> findNumbers(String json) {
        Pattern pattern = Pattern.compile("-?\\d+");
        Matcher matcher = pattern.matcher(json);

        List<Integer> numbers = new ArrayList<>();
        while (matcher.find()) {
            numbers.add(Integer.parseInt(matcher.group(0)));
        }
        return Array.ofAll(numbers);
    }

    public int part2(String input) {
        return Function1.<String, Integer>of(this::findAndFilterRed)
                .apply(input);
    }

    private int findAndFilterRed(String json) {
        ObjectMapper mapper = new ObjectMapper();
        return Try.of(() -> mapper.readTree(json)).map(this::findAndFilterRed).getOrElse(0);
    }

    private int findAndFilterRed(JsonNode node) {
        if (node.isInt()) {
            return node.asInt();
        }
        if (node.isTextual()) {
            return 0;
        }
        if (node.isObject()) {
            if (node.properties().stream()
                    .map(Map.Entry::getValue)
                    .filter(JsonNode::isTextual)
                    .map(JsonNode::asText)
                    .anyMatch("red"::equals)) {
                return 0;
            }
            return node.properties().stream()
                    .map(Map.Entry::getValue)
                    .mapToInt(this::findAndFilterRed)
                    .sum();
        }
        return Array.ofAll(node).map(this::findAndFilterRed).sum().intValue();
    }

}
