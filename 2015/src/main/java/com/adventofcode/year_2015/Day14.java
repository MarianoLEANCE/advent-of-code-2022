package com.adventofcode.year_2015;

import com.adventofcode.common.ArrayUtils;
import com.adventofcode.common.StringUtils;
import io.vavr.Function1;
import io.vavr.Tuple2;
import io.vavr.collection.Array;
import io.vavr.collection.Map;
import io.vavr.collection.Traversable;

public class Day14 {

    public int part1(String input, int duration) {
        return Function1.of(StringUtils.split("\n"))
                .andThen(ArrayUtils.map(this::parse))
                .andThen(ArrayUtils.map(tickNTimes(duration)))
                .andThen(ArrayUtils.map(Rein::distanceCovered))
                .andThen(Traversable::max)
                .andThen(m -> m.getOrElse(0))
                .apply(input);
    }

    private Function1<Rein, Rein> tickNTimes(int times) {
        return Array.range(0, times).map(x -> Function1.of(this::tick)).reduce(Function1::andThen);
    }

    private Rein parse(String line) {
        String[] chunks = line.split(" ");
        String name = chunks[0];
        int flyingSpeed = Integer.parseInt(chunks[3]);
        int flyingDuration = Integer.parseInt(chunks[6]);
        int restingDuration = Integer.parseInt(chunks[13]);
        return new Rein(name, flyingSpeed, flyingDuration, restingDuration, 0, 0);
    }

    private record Rein(String name,
                        int flyingSpeed,
                        int flyingDuration,
                        int restingDuration,
                        int distanceCovered,
                        int secondsElapsed) {}

    private Rein tick(Rein rein) {
        int state = rein.secondsElapsed % (rein.flyingDuration + rein.restingDuration);
        int nextDistanceCovered = state < rein.flyingDuration
                ? rein.distanceCovered + rein.flyingSpeed
                : rein.distanceCovered;
        int nextSecondsElapsed = rein.secondsElapsed + 1;
        return new Rein(rein.name, rein.flyingSpeed, rein.flyingDuration, rein.restingDuration,
                nextDistanceCovered, nextSecondsElapsed);
    }

    private Array<Rein> eachSecond(Rein rein, int duration) {
        Rein nextState = tick(rein);
        if (duration == 1)
            return Array.of(tick(nextState));
        return Array.of(nextState).appendAll(eachSecond(nextState, duration - 1));
    }

    public int part2(String input, int duration) {
        return Function1.of(StringUtils.split("\n"))
                .andThen(ArrayUtils.map(this::parse))
                .andThen(ArrayUtils.map(rein -> eachSecond(rein, duration)))
                .andThen(this::leadAtEachSecond)
                .andThen(this::bestScore)
                .apply(input);
    }

    private Map<String, Integer> leadAtEachSecond(Array<Array<Rein>> states) {
        var winners = Array.range(0, states.head().size()).flatMap(i -> leadAtSecond(states, i));
        var distinctNames = winners.map(Rein::name).distinct();
        return distinctNames.toMap(name -> new Tuple2<>(name, winners.count(rein -> rein.name.equals(name))));
    }

    private int bestScore(Map<String, Integer> scores) {
        return scores.values().max().getOrElse(0);
    }

    private Array<Rein> leadAtSecond(Array<Array<Rein>> states, int instant) {
        Array<Rein> reins = states.map(arr -> arr.get(instant));
        int bestDistance = bestDistance(reins);
        return reins.filter(rein -> rein.distanceCovered() == bestDistance);
    }

    private int bestDistance(Array<Rein> reins) {
        return reins.map(Rein::distanceCovered).max().getOrElse(0);
    }

}
