package com.adventofcode.year_2015;

import com.adventofcode.common.ArrayUtils;
import com.adventofcode.common.CombinationUtils;
import com.adventofcode.common.StringUtils;
import io.vavr.Function1;
import io.vavr.Function2;
import io.vavr.Tuple2;
import io.vavr.collection.Array;

public class Day17 {

    public long part1(String input, int total) {
        return Function1.of(StringUtils.split("\n"))
                .andThen(ArrayUtils.map(Integer::parseInt))
                .andThen(Function2.of(this::storageCombinations).apply(total))
                .apply(input);
    }

    private long storageCombinations(int left, Array<Integer> containers) {
        return matches(left, containers).size();
    }

    public long part2(String input, int total) {
        return Function1.of(StringUtils.split("\n"))
                .andThen(ArrayUtils.map(Integer::parseInt))
                .andThen(Function2.of(this::minContainersCount).apply(total))
                .apply(input);
    }

    private long minContainersCount(int left, Array<Integer> containers) {
        Array<Array<Integer>> matches = matches(left, containers);
        int min = matches.map(Array::length).min().getOrElse(0);
        return matches.count(combination -> combination.size() == min);
    }

    private Array<Array<Integer>> matches(int left, Array<Integer> containers) {
        return CombinationUtils.combinations(Array.of(true, false), containers.size())
                .map(combination -> combination.zip(containers)
                        .filter(Tuple2::_1)
                        .map(Tuple2::_2))
                .filter(combination -> combination.sum().intValue() == left);
    }

}
