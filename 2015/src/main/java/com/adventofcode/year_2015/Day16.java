package com.adventofcode.year_2015;

import com.adventofcode.common.ArrayUtils;
import com.adventofcode.common.StringUtils;
import io.vavr.Function1;
import io.vavr.collection.Array;

import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day16 {

    public long part1(String input) {
        return Function1.of(StringUtils.split("\n"))
                .andThen(ArrayUtils.map(Aunt::new))
                .andThen(ArrayUtils.filter(auntFilter(Aunt::children, 3)))
                .andThen(ArrayUtils.filter(auntFilter(Aunt::cats, 7)))
                .andThen(ArrayUtils.filter(auntFilter(Aunt::samoyeds, 2)))
                .andThen(ArrayUtils.filter(auntFilter(Aunt::pomeranians, 3)))
                .andThen(ArrayUtils.filter(auntFilter(Aunt::akitas, 0)))
                .andThen(ArrayUtils.filter(auntFilter(Aunt::vizslas, 0)))
                .andThen(ArrayUtils.filter(auntFilter(Aunt::goldfish, 5)))
                .andThen(ArrayUtils.filter(auntFilter(Aunt::trees, 3)))
                .andThen(ArrayUtils.filter(auntFilter(Aunt::cars, 2)))
                .andThen(ArrayUtils.filter(auntFilter(Aunt::perfumes, 1)))
                .andThen(ArrayUtils.map(Aunt::id))
                .andThen(Array::head)
                .apply(input);
    }

    public long part2(String input) {
        return Function1.of(StringUtils.split("\n"))
                .andThen(ArrayUtils.map(Aunt::new))
                .andThen(ArrayUtils.filter(auntFilter(Aunt::children, 3)))
                .andThen(ArrayUtils.filter(auntFilterGt(Aunt::cats, 7)))
                .andThen(ArrayUtils.filter(auntFilter(Aunt::samoyeds, 2)))
                .andThen(ArrayUtils.filter(auntFilterLt(Aunt::pomeranians, 3)))
                .andThen(ArrayUtils.filter(auntFilter(Aunt::akitas, 0)))
                .andThen(ArrayUtils.filter(auntFilter(Aunt::vizslas, 0)))
                .andThen(ArrayUtils.filter(auntFilterLt(Aunt::goldfish, 5)))
                .andThen(ArrayUtils.filter(auntFilterGt(Aunt::trees, 3)))
                .andThen(ArrayUtils.filter(auntFilter(Aunt::cars, 2)))
                .andThen(ArrayUtils.filter(auntFilter(Aunt::perfumes, 1)))
                .andThen(ArrayUtils.map(Aunt::id))
                .andThen(Array::head)
                .apply(input);
    }

    private record Aunt(Integer id,
                        Integer children,
                        Integer cats,
                        Integer samoyeds,
                        Integer pomeranians,
                        Integer akitas,
                        Integer vizslas,
                        Integer goldfish,
                        Integer trees,
                        Integer cars,
                        Integer perfumes) {

        Aunt(String s) {
            this(parse(s, "Sue (\\d+):.*"),
                    parse(s, ".*children: (\\d+).*"),
                    parse(s, ".*cats: (\\d+).*"),
                    parse(s, ".*samoyeds: (\\d+).*"),
                    parse(s, ".*pomeranians: (\\d+).*"),
                    parse(s, ".*akitas: (\\d+).*"),
                    parse(s, ".*vizslas: (\\d+).*"),
                    parse(s, ".*goldfish: (\\d+).*"),
                    parse(s, ".*trees: (\\d+).*"),
                    parse(s, ".*cars: (\\d+).*"),
                    parse(s, ".*perfumes: (\\d+).*"));
        }

        static Integer parse(String input, String regex) {
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(input);
            return matcher.matches() ?
                    Integer.parseInt(matcher.group(1)) :
                    null;
        }
    }

    private Predicate<Aunt> auntFilter(Function1<Aunt, Integer> property, Integer value) {
        return aunt -> {
            Integer x = property.apply(aunt);
            return x == null || x.equals(value);
        };
    }

    private Predicate<Aunt> auntFilterGt(Function1<Aunt, Integer> property, Integer value) {
        return aunt -> {
            Integer x = property.apply(aunt);
            return x == null || x > value;
        };
    }

    private Predicate<Aunt> auntFilterLt(Function1<Aunt, Integer> property, Integer value) {
        return aunt -> {
            Integer x = property.apply(aunt);
            return x == null || x < value;
        };
    }

}
