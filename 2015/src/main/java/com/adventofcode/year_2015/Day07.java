package com.adventofcode.year_2015;

import io.vavr.Function3;
import io.vavr.collection.Array;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Day07 {

    public char part1(String input, String wire) {
        return Function3.of(this::resolve).curried().apply(parse(input)).apply(new ConcurrentHashMap<>()).apply(wire);
    }

    private Array<String> parse(String input) {
        return Array.of(input.split("\n"));
    }

    private char resolve(Array<String> instructions, Map<String, Character> cache, String wire) {
        if (cache.containsKey(wire)) {
            return cache.get(wire);
        }
        if (wire.matches("\\d+")) {
            return (char) Integer.parseInt(wire);
        }

        String instruction = instructions.filter(i -> i.endsWith(" -> " + wire)).get().split(" -> ")[0];

        if (instruction.contains(" AND ")) {
            String[] operands = instruction.split(" AND ");
            char resultA = resolve(instructions, cache, operands[0]);
            cache.put(operands[0], resultA);
            char resultB = resolve(instructions, cache, operands[1]);
            cache.put(operands[1], resultB);
            return (char) (resultA & resultB);
        }

        if (instruction.contains(" OR ")) {
            String[] operands = instruction.split(" OR ");
            char resultA = resolve(instructions, cache, operands[0]);
            cache.put(operands[0], resultA);
            char resultB = resolve(instructions, cache, operands[1]);
            cache.put(operands[1], resultB);
            return (char) (resultA | resultB);
        }

        if (instruction.contains("NOT ")) {
            String operand = instruction.substring("NOT ".length());
            char resultA = resolve(instructions, cache, operand);
            cache.put(operand, resultA);
            return (char) ~resultA;
        }

        if (instruction.contains(" RSHIFT ")) {
            String[] operands = instruction.split(" RSHIFT ");
            char resultA = resolve(instructions, cache, operands[0]);
            cache.put(operands[0], resultA);
            int shiftBy = Integer.parseInt(operands[1]);
            return (char) (resultA >> shiftBy);
        }

        if (instruction.contains(" LSHIFT ")) {
            String[] operands = instruction.split(" LSHIFT ");
            char resultA = resolve(instructions, cache, operands[0]);
            cache.put(operands[0], resultA);
            int shiftBy = Integer.parseInt(operands[1]);
            return (char) (resultA << shiftBy);
        }

        if (instruction.matches("[a-z]+")) {
            char resultA = resolve(instructions, cache, instruction);
            cache.put(instruction, resultA);
            return resolve(instructions, cache, instruction);
        }

        char resultA = (char) Integer.parseInt(instruction);
        cache.put(instruction, resultA);
        return resultA;
    }

    public char part2(String input, String wire) {
        String overriden = input.replace("14146 -> b", "956 -> b");
        return Function3.of(this::resolve).curried().apply(parse(overriden)).apply(new ConcurrentHashMap<>()).apply(wire);
    }

}
