package com.adventofcode.year_2015;

import com.adventofcode.common.ArrayUtils;
import com.adventofcode.common.StringUtils;
import io.vavr.Function1;
import io.vavr.collection.Traversable;

public class Day08 {

    public int part1(String input) {
        return Function1.of(StringUtils.split("\n"))
                .andThen(ArrayUtils.map(this::diffUnescape))
                .andThen(Traversable::sum)
                .andThen(Number::intValue)
                .apply(input);
    }

    private Number diffUnescape(String line) {
        return stringLiteralLength(line) - stringMemoryLength(line);
    }

    private int stringLiteralLength(String line) {
        return line.length();
    }

    private int stringMemoryLength(String line) {
        String quotesReplaced = line.replaceAll("\"(.*)\"", "$1");
        String backslashReplaced = quotesReplaced.replaceAll("\\\\\\\\", "_");
        String hexReplaced = backslashReplaced.replaceAll("\\\\x[0-9a-f]{2}", "_");
        String singleReplaced = hexReplaced.replaceAll("\\\\.", "_");
        return singleReplaced.length();
    }

    public int part2(String input) {
        return Function1.of(StringUtils.split("\n"))
                .andThen(ArrayUtils.map(this::diffEscape))
                .andThen(Traversable::sum)
                .andThen(Number::intValue)
                .apply(input);
    }

    private Number diffEscape(String line) {
        return encodedMemoryLength(line) - stringLiteralLength(line);
    }

    private int encodedMemoryLength(String line) {
        String quotesEscaped = line.replaceAll("\"", "__");
        String backslashEscaped = quotesEscaped.replaceAll("\\\\", "__");
        return backslashEscaped.length() + 2;
    }

}
