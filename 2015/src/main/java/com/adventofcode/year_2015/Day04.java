package com.adventofcode.year_2015;

import com.adventofcode.common.StringUtils;
import io.vavr.Function1;
import io.vavr.Function2;
import org.apache.commons.codec.digest.DigestUtils;

import java.util.stream.IntStream;

public class Day04 {

    public int part1(String input) {
        return Function1.of(StringUtils.replace("\n", ""))
                .andThen(Function2.of(this::findHash).curried().apply("0".repeat(5)))
                .apply(input);
    }

    public int part2(String input) {
        return Function1.of(StringUtils.replace("\n", ""))
                .andThen(Function2.of(this::findHash).curried().apply("0".repeat(6)))
                .apply(input);
    }

    private int findHash(String prefix, String input) {
        return IntStream.rangeClosed(0, Integer.MAX_VALUE)
                .filter(nonce -> matches(prefix, nonce, input))
                .findFirst()
                .getAsInt();
    }

    private boolean matches(String prefix, int nonce, String input) {
        String padded = input + nonce;
        return DigestUtils.md5Hex(padded).startsWith(prefix);
    }

}
