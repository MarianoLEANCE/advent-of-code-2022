package com.adventofcode.year_2015;

import com.adventofcode.common.StringUtils;
import io.vavr.Function1;
import io.vavr.collection.Array;

public class Day10 {

    public int part1(String input) {
        return lookAndSayTimes(input, 40);
    }

    public int part2(String input) {
        return lookAndSayTimes(input, 50);
    }

    public int lookAndSayTimes(String input, int times) {
        return Function1.of(StringUtils.replace("\n", ""))
                .andThen(lookAndSayTimes(times))
                .andThen(String::length)
                .apply(input);
    }

    private Function1<String, String> lookAndSayTimes(int times) {
        return Array.range(0, times).map(i -> Function1.of(this::lookAndSay))
                .reduce(Function1::andThen);
    }

    private String lookAndSay(String s) {
        int i = 0;
        StringBuilder sb = new StringBuilder();
        while (i < s.length()) {
            int count = 0;
            char c = s.charAt(i);
            do {
                i++;
                count++;
            } while (i < s.length() && s.charAt(i) == c);
            sb.append(count);
            sb.append(c);
        }
        return sb.toString();
    }

}
