package com.adventofcode.year_2015;

import com.adventofcode.common.ArrayUtils;
import com.adventofcode.common.StringUtils;
import io.vavr.Function1;
import io.vavr.Function3;
import io.vavr.Tuple2;
import io.vavr.Tuple3;
import io.vavr.collection.Array;

public class Day13 {

    public int part1(String input) {
        return Function1.of(StringUtils.split("\n"))
                .andThen(ArrayUtils.map(this::parse))
                .andThen(this::maxHappiness)
                .apply(input);
    }

    private Tuple3<String, String, Integer> parse(String line) {
        String[] chunks = line.split(" ");
        String a = chunks[0];
        String b = chunks[10].replace(".", "");
        boolean gain = chunks[2].equals("gain");
        int change = Integer.parseInt(chunks[3]);
        return new Tuple3<>(a, b, gain ? change : -change);
    }

    private int maxHappiness(Array<Tuple3<String, String, Integer>> preferences) {
        Array<String> persons = preferences.flatMap(line -> Array.of(line._1(), line._2()))
                .distinct();
        Array<Array<String>> permutations = persons.permutations();
        return permutations.map(permutation -> computeHappiness(preferences, permutation))
                .max()
                .getOrElse(0);
    }

    private int computeHappiness(Array<Tuple3<String, String, Integer>> distances, Array<String> permutation) {
        var distance = Function3.of(this::getHappiness).apply(distances).memoized();
        Array<Tuple2<String, String>> segments = ring(permutation);
        return segments.map(segment -> distance.apply(segment._1(), segment._2())).sum().intValue();
    }

    private Array<Tuple2<String, String>> ring(Array<String> arrangement) {
        Array<String> rotate = arrangement.subSequence(1).append(arrangement.get(0));
        return arrangement.zip(rotate);
    }

    private int getHappiness(Array<Tuple3<String, String, Integer>> arrangements, String guestA, String guestB) {
        return arrangements.filter(arrangement -> (arrangement._1().equals(guestA) && arrangement._2().equals(guestB))
                                                  || (arrangement._1().equals(guestB) && arrangement._2().equals(guestA)))
                .map(Tuple3::_3)
                .sum()
                .intValue();
    }

    public int part2(String input) {
        return Function1.of(StringUtils.split("\n"))
                .andThen(ArrayUtils.map(this::parse))
                .andThen(this::maxHappinessWithMe)
                .apply(input);
    }

    private int maxHappinessWithMe(Array<Tuple3<String, String, Integer>> preferences) {
        Array<String> persons = preferences.flatMap(line -> Array.of(line._1(), line._2()))
                .distinct().append("me");
        Array<Array<String>> permutations = persons.permutations();
        return permutations.map(permutation -> computeHappiness(preferences, permutation))
                .max()
                .getOrElse(0);
    }

}
