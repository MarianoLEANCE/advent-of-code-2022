package com.adventofcode.year_2015;

import com.adventofcode.common.StringUtils;
import io.vavr.Function1;
import io.vavr.Tuple2;
import io.vavr.collection.Array;

public class Day11 {

    public String part1(String input) {
        return nextValidPassword(input.replace("\n", ""));
    }

    public String part2(String input) {
     return    Function1.of(StringUtils.replace("\n", ""))
                .andThen(this::nextValidPassword)
                .andThen(this::nextValidPassword)
                .apply(input);
    }

    private String nextValidPassword(String password) {
        String incremented = increment(password);
        while (!isValid(incremented)) {
            incremented = increment(incremented);
        }
        return incremented;
    }

    private String increment(String password) {
        return Function1.of(this::toArray)
                .andThen(this::increment)
                .andThen(this::fromArray)
                .apply(password);
    }

    private Array<Character> increment(Array<Character> password) {
        Character tail = password.last();
        Array<Character> left = password.dropRight(1);
        return tail.equals('z')
                ? increment(left).append('a')
                : left.append((char) (tail + 1));
    }

    private Array<Character> toArray(String s) {
        return Array.ofAll(s.toCharArray());
    }

    private String fromArray(Array<Character> array) {
        return array.map(Object::toString).reduce(String::concat);
    }

    private boolean isValid(String password) {
        return Array.of(Function1.of(this::threeIncreasing),
                        Function1.of(this::blackList),
                        Function1.of(this::pairs))
                .map(condition -> condition.apply(password))
                .reduce(Boolean::logicalAnd);
    }

    private boolean threeIncreasing(String password) {
        return Array.range(0, password.length() - 2)
                .map(i -> password.charAt(i) + 1 == password.charAt(i + 1) &&
                          password.charAt(i) + 2 == password.charAt(i + 2))
                .reduce(Boolean::logicalOr);
    }

    private boolean blackList(String password) {
        return !(password.contains("i") || password.contains("o") || password.contains("l"));
    }

    private boolean pairs(String password) {
        Array<Character> characters = Array.ofAll(password.toCharArray());
        return characters.drop(1).zip(characters.dropRight(1))
                       .filter(tuple -> tuple._1().equals(tuple._2()))
                       .map(Tuple2::_1)
                       .distinct()
                       .size() > 1;
    }

}
