package com.adventofcode.year_2015;

import com.adventofcode.common.InputUtils;
import io.vavr.control.Try;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day17Test {

    Day17 day = new Day17();

    @Test
    void part1Example() {
        assertEquals(4, day.part1(example(), 25));
    }

    @Test
    void part1() {
        long expected = input().map(input -> day.part1(input, 150)).get();
        assertEquals(1304, expected);
    }

    @Test
    void part2Example() {
        assertEquals(3, day.part2(example(), 25));
    }

    @Test
    void part2() {
        long expected = input().map(input -> day.part2(input, 150)).get();
        assertEquals(18, expected);
    }

    private String example() {
        return """
                20
                15
                10
                5
                5
                """;
    }

    private Try<String> input() {
        return InputUtils.readFromClassPath("day17.txt");
    }

}
