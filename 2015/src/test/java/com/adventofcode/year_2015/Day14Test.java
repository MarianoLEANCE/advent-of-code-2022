package com.adventofcode.year_2015;

import com.adventofcode.common.InputUtils;
import io.vavr.control.Try;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day14Test {

    Day14 day = new Day14();

    @Test
    void part1Example() {
        assertEquals(1120, day.part1(example(), 1000));
    }

    @Test
    void part1() {
        int expected = input().map(input -> day.part1(input, 2503)).get();
        assertEquals(2655, expected);
    }

    @Test
    void part2Example() {
        assertEquals(689, day.part2(example(), 1000));
    }

    @Test
    void part2() {
        int expected = input().map(input -> day.part2(input, 2503)).get();
        assertEquals(1059, expected);
    }

    private String example() {
        return """
                Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.
                Dancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds.
                """;
    }

    private Try<String> input() {
        return InputUtils.readFromClassPath("day14.txt");
    }

}
