package com.adventofcode.year_2015;

import com.adventofcode.common.InputUtils;
import io.vavr.control.Try;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day09Test {

    Day09 day = new Day09();

    @Test
    void part1Example() {
        String example = """
                         London to Dublin = 464
                         London to Belfast = 518
                         Dublin to Belfast = 141
                         """;
        assertEquals(605, day.part1(example));
    }

    @Test
    void part1() {
        int expected = input().map(input -> day.part1(input)).get();
        assertEquals(251, expected);
    }

    @Test
    void part2Example() {
        String example = """
                         London to Dublin = 464
                         London to Belfast = 518
                         Dublin to Belfast = 141
                         """;
        assertEquals(982, day.part2(example));
    }

    @Test
    void part2() {
        int expected = input().map(input -> day.part2(input)).get();
        assertEquals(898, expected);
    }

    private Try<String> input() {
        return InputUtils.readFromClassPath("day09.txt");
    }

}
