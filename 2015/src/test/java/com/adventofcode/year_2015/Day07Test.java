package com.adventofcode.year_2015;

import com.adventofcode.common.InputUtils;
import io.vavr.control.Try;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day07Test {

    Day07 day = new Day07();

    @Test
    void part1Example() {
        String example = """
                123 -> x
                456 -> y
                x AND y -> d
                x OR y -> e
                x LSHIFT 2 -> f
                y RSHIFT 2 -> g
                NOT x -> h
                NOT y -> i
                """;
        assertEquals(72, day.part1(example, "d"));
        assertEquals(507, day.part1(example, "e"));
        assertEquals(114, day.part1(example, "g"));
        assertEquals(65412, day.part1(example, "h"));
        assertEquals(65079, day.part1(example, "i"));
        assertEquals(123, day.part1(example, "x"));
        assertEquals(456, day.part1(example, "y"));
    }

    @Test
    void part1() {
        int expected = input().map(input -> day.part1(input, "a")).get();
        assertEquals(956, expected);
    }

    @Test
    void part2() {
        int expected = input().map(input -> day.part2(input, "a")).get();
        assertEquals(40149, expected);
    }

    private Try<String> input() {
        return InputUtils.readFromClassPath("day07.txt");
    }

}
