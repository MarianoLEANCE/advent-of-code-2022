package com.adventofcode.year_2015;

import com.adventofcode.common.InputUtils;
import io.vavr.control.Try;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day12Test {

    Day12 day = new Day12();

    @Test
    void part1() {
        int expected = input().map(input -> day.part1(input)).get();
        assertEquals(119433, expected);
    }

    @Test
    void part1Example() {
        assertEquals(6, day.part1("[1,2,3]"));
        assertEquals(6, day.part1("{\"a\":2,\"b\":4}"));
        assertEquals(3, day.part1("[[[3]]]"));
        assertEquals(3, day.part1("{\"a\":{\"b\":4},\"c\":-1}"));
        assertEquals(0, day.part1("{\"a\":[-1,1]}"));
        assertEquals(0, day.part1("[-1,{\"a\":1}]"));
        assertEquals(0, day.part1("[]"));
        assertEquals(0, day.part1("{}"));
    }

    @Test
    void part2Example() {
        assertEquals(6, day.part2("[1,2,3]"));
        assertEquals(4, day.part2("[1,{\"c\":\"red\",\"b\":2},3]"));
        assertEquals(0, day.part2("{\"d\":\"red\",\"e\":[1,2,3,4],\"f\":5}"));
        assertEquals(6, day.part2("[1,\"red\",5]"));
        assertEquals(6, day.part2("{\"a\":1,\"b\":{\"c\":\"red\",\"b\":2},\"c\":3,\"d\":[\"red\",2]}"));
        assertEquals(6, day.part2("{\"a\":1,\"b\":[\"red\",\"blue\",5]}"));
    }

    @Test
    void part2() {
        int expected = input().map(input -> day.part2(input)).get();
        assertEquals(68466, expected);
    }

    private Try<String> input() {
        return InputUtils.readFromClassPath("day12.txt");
    }

}
