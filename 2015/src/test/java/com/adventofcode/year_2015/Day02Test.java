package com.adventofcode.year_2015;

import com.adventofcode.common.InputUtils;
import io.vavr.control.Try;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day02Test {

    Day02 day = new Day02();

    @Test
    void part1Example() {
        int expected = day.part1("2x3x4");
        assertEquals(58, expected);
    }

    @Test
    void part1() {
        int expected = input().map(day::part1).get();
        assertEquals(1598415, expected);
    }

    @Test
    void part2Example1() {
        int expected = day.part2("2x3x4");
        assertEquals(34, expected);
    }

    @Test
    void part2Example2() {
        int expected = day.part2("1x1x10");
        assertEquals(14, expected);
    }

    @Test
    void part2() {
        int expected = input().map(day::part2).get();
        assertEquals(3812909, expected);
    }

    private Try<String> input() {
        return InputUtils.readFromClassPath("day02.txt");
    }

}
