package com.adventofcode.year_2015;

import com.adventofcode.common.InputUtils;
import io.vavr.control.Try;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day15Test {

    Day15 day = new Day15();

    @Test
    void part1Example() {
        assertEquals(62842880, day.part1(example()));
    }

    @Test
    void part1() {
        long expected = input().map(input -> day.part1(input)).get();
        assertEquals(13882464, expected);
    }

    @Test
    void part2Example() {
        assertEquals(57600000, day.part2(example()));
    }

    @Test
    void part2() {
        long expected = input().map(input -> day.part2(input)).get();
        assertEquals(11171160, expected);
    }

    private String example() {
        return """
                Butterscotch: capacity -1, durability -2, flavor 6, texture 3, calories 8
                Cinnamon: capacity 2, durability 3, flavor -2, texture -1, calories 3
                """;
    }

    private Try<String> input() {
        return InputUtils.readFromClassPath("day15.txt");
    }

}
