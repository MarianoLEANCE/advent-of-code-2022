package com.adventofcode.year_2015;

import com.adventofcode.common.InputUtils;
import io.vavr.control.Try;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day13Test {

    Day13 day = new Day13();

    @Test
    void part1Example() {
        assertEquals(330, day.part1(example()));
    }

    @Test
    void part1() {
        int expected = input().map(input -> day.part1(input)).get();
        assertEquals(709, expected);
    }

    @Test
    void part2() {
        int expected = input().map(input -> day.part2(input)).get();
        assertEquals(668, expected);
    }

    private String example() {
        return """
                Alice would gain 54 happiness units by sitting next to Bob.
                Alice would lose 79 happiness units by sitting next to Carol.
                Alice would lose 2 happiness units by sitting next to David.
                Bob would gain 83 happiness units by sitting next to Alice.
                Bob would lose 7 happiness units by sitting next to Carol.
                Bob would lose 63 happiness units by sitting next to David.
                Carol would lose 62 happiness units by sitting next to Alice.
                Carol would gain 60 happiness units by sitting next to Bob.
                Carol would gain 55 happiness units by sitting next to David.
                David would gain 46 happiness units by sitting next to Alice.
                David would lose 7 happiness units by sitting next to Bob.
                David would gain 41 happiness units by sitting next to Carol.
                """;
    }

    private Try<String> input() {
        return InputUtils.readFromClassPath("day13.txt");
    }

}
