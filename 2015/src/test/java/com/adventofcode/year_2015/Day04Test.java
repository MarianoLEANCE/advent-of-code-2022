package com.adventofcode.year_2015;

import com.adventofcode.common.InputUtils;
import io.vavr.control.Try;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day04Test {

    Day04 day = new Day04();

    @Test
    void part1Example1() {
        int expected = day.part1("abcdef");
        assertEquals(609043, expected);
    }

    @Test
    void part1() {
        int expected = input().map(day::part1).get();
        assertEquals(254575, expected);
    }

    @Test
    void part2() {
        int expected = input().map(day::part2).get();
        assertEquals(1038736, expected);
    }

    private Try<String> input() {
        return InputUtils.readFromClassPath("day04.txt");
    }

}
