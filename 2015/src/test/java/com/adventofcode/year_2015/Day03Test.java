package com.adventofcode.year_2015;

import com.adventofcode.common.InputUtils;
import io.vavr.control.Try;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day03Test {

    Day03 day = new Day03();

    @Test
    void part1Example1() {
        int expected = day.part1(">");
        assertEquals(2, expected);
    }

    @Test
    void part1Example2() {
        int expected = day.part1("^>v<");
        assertEquals(4, expected);
    }

    @Test
    void part1Example3() {
        int expected = day.part1("^v^v^v^v^v");
        assertEquals(2, expected);
    }

    @Test
    void part1() {
        int expected = input().map(day::part1).get();
        assertEquals(2565, expected);
    }

    @Test
    void part2Example1() {
        int expected = day.part2("^v");
        assertEquals(3, expected);
    }

    @Test
    void part2Example2() {
        int expected = day.part2("^>v<");
        assertEquals(3, expected);
    }

    @Test
    void part2Example3() {
        int expected = day.part2("^v^v^v^v^v");
        assertEquals(11, expected);
    }

    @Test
    void part2() {
        int expected = input().map(day::part2).get();
        assertEquals(2639, expected);
    }

    private Try<String> input() {
        return InputUtils.readFromClassPath("day03.txt");
    }

}
