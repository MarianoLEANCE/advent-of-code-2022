package com.adventofcode.year_2015;

import com.adventofcode.common.InputUtils;
import io.vavr.control.Try;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day06Test {

    Day06 day = new Day06();

    @Test
    void part1Example() {
        String example = """
                turn on 0,0 through 999,999
                toggle 1,1 through 998,998
                """;
        int expected = day.part1(example);
        assertEquals(4 * 999, expected);
    }

    @Test
    void part1() {
        int expected = input().map(day::part1).get();
        assertEquals(543903, expected);
    }

    @Test
    void part2Example() {
        String example = """
                turn on 0,0 through 999,999
                turn on 0,0 through 999,999
                turn toggle 0,0 through 999,999
                turn off 0,0 through 0,0
                """;
        long expected = day.part2(example);
        assertEquals(4_000_000 - 1, expected);
    }

    @Test
    void part2() {
        long expected = input().map(day::part2).get();
        assertEquals(14687245, expected);
    }

    private Try<String> input() {
        return InputUtils.readFromClassPath("day06.txt");
    }

}
