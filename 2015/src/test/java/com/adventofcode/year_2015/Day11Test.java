package com.adventofcode.year_2015;

import com.adventofcode.common.InputUtils;
import io.vavr.control.Try;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day11Test {

    Day11 day = new Day11();

    @Test
    void part1() {
        String expected = input().map(input -> day.part1(input)).get();
        assertEquals("vzbxxyzz", expected);
    }

    @Test
    void part2() {
        String expected = input().map(input -> day.part2(input)).get();
        assertEquals("vzcaabcc", expected);
    }

    private Try<String> input() {
        return InputUtils.readFromClassPath("day11.txt");
    }

}
