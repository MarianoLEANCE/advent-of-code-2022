package com.adventofcode.year_2015;

import com.adventofcode.common.InputUtils;
import io.vavr.control.Try;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day16Test {

    Day16 day = new Day16();

    @Test
    void part1() {
        long expected = input().map(input -> day.part1(input)).get();
        assertEquals(103, expected);
    }

    @Test
    void part2() {
        long expected = input().map(input -> day.part2(input)).get();
        assertEquals(405, expected);
    }

    private Try<String> input() {
        return InputUtils.readFromClassPath("day16.txt");
    }

}
