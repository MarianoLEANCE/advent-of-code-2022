package com.adventofcode.year_2015;

import com.adventofcode.common.InputUtils;
import io.vavr.control.Try;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day05Test {

    Day05 day = new Day05();

    @Test
    void part1Example1() {
        int expected = day.part1("ugknbfddgicrmopn");
        assertEquals(1, expected);
    }

    @Test
    void part1Example2() {
        int expected = day.part1("aaa");
        assertEquals(1, expected);
    }

    @Test
    void part1Example3() {
        int expected = day.part1("jchzalrnumimnmhp");
        assertEquals(0, expected);
    }

    @Test
    void part1Example4() {
        int expected = day.part1("haegwjzuvuyypxyu");
        assertEquals(0, expected);
    }

    @Test
    void part1Example5() {
        int expected = day.part1("dvszwmarrgswjxmb");
        assertEquals(0, expected);
    }

    @Test
    void part1() {
        int expected = input().map(day::part1).get();
        assertEquals(238, expected);
    }

    @Test
    void part2Example1() {
        int expected = day.part2("qjhvhtzxzqqjkmpb");
        assertEquals(1, expected);
    }

    @Test
    void part2Example2() {
        int expected = day.part2("xxyxx");
        assertEquals(1, expected);
    }

    @Test
    void part2Example3() {
        int expected = day.part2("uurcxstgmygtbstg");
        assertEquals(0, expected);
    }

    @Test
    void part2Example4() {
        int expected = day.part2("ieodomkazucvgmuy");
        assertEquals(0, expected);
    }

    @Test
    void part2() {
        int expected = input().map(day::part2).get();
        assertEquals(69, expected);
    }

    private Try<String> input() {
        return InputUtils.readFromClassPath("day05.txt");
    }

}
