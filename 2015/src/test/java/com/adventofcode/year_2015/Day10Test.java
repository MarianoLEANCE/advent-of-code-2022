package com.adventofcode.year_2015;

import com.adventofcode.common.InputUtils;
import io.vavr.control.Try;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day10Test {

    Day10 day = new Day10();

    @Test
    void part1Example() {
        assertEquals(2, day.lookAndSayTimes("1", 1));
        assertEquals(2, day.lookAndSayTimes("1", 2));
        assertEquals(4, day.lookAndSayTimes("1", 3));
        assertEquals(6, day.lookAndSayTimes("1", 4));
        assertEquals(6, day.lookAndSayTimes("1", 5));
    }

    @Test
    void part1() {
        int expected = input().map(input -> day.part1(input)).get();
        assertEquals(329356, expected);
    }

    @Test
    void part2() {
        int expected = input().map(input -> day.part2(input)).get();
        assertEquals(4666278, expected);
    }

    private Try<String> input() {
        return InputUtils.readFromClassPath("day10.txt");
    }

}
