package com.adventofcode.year_2015;

import com.adventofcode.common.InputUtils;
import io.vavr.control.Try;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day08Test {

    Day08 day = new Day08();

    @Test
    void part1Example() {
        String example = """
                ""
                "abc"
                "aaa\\"aaa"
                "\\x27"
                """;
        assertEquals(12, day.part1(example));
    }

    @Test
    void part1() {
        int expected = input().map(input -> day.part1(input)).get();
        assertEquals(1342, expected);
    }

    @Test
    void part2Example() {
        String example = """
                ""
                "abc"
                "aaa\\"aaa"
                "\\x27"
                """;
        assertEquals(19, day.part2(example));
    }

    @Test
    void part2() {
        int expected = input().map(input -> day.part2(input)).get();
        assertEquals(2074, expected);
    }

    private Try<String> input() {
        return InputUtils.readFromClassPath("day08.txt");
    }

}
