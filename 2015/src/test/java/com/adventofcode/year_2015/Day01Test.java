package com.adventofcode.year_2015;

import com.adventofcode.common.InputUtils;
import io.vavr.control.Try;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day01Test {

    Day01 day = new Day01();

    @Test
    void part1Example() {
        int expected = day.part1("()()");
        assertEquals(0, expected);
    }

    @Test
    void part1() {
        int expected = input().map(day::part1).get();
        assertEquals(74, expected);
    }

    @Test
    void part2Example1() {
        int expected = day.part2(")");
        assertEquals(1, expected);
    }

    @Test
    void part2Example2() {
        int expected = day.part2("()())");
        assertEquals(5, expected);
    }

    @Test
    void part2() {
        int expected = input().map(day::part2).get();
        assertEquals(1795, expected);
    }

    private Try<String> input() {
        return InputUtils.readFromClassPath("day01.txt");
    }

}
